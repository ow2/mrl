<!--
SPDX-FileCopyrightText: 2021 OW2

SPDX-License-Identifier: Apache-2.0
-->

# What is MRL?

MRL (Market Readiness Levels) refers to a project assessment methodology initiated by OW2, and described here: https://www.ow2.org/view/MRL .

MRL includes, but is not limited to, a technical layer that collects data from different sources (issue trackers including Gitlab/Github/Jira, SonarQube, Scancode...), aggregates them as significant metrics (like the number of unanswered bugs, or the ratio of source files without license), then allows to visualize them.

The current repository contains the technical framework related to data collection, aggregation and visualization.

# MRL data collection

Move to data-collection/ directory, then build with maven:

```
mvn clean install
```

## Quick start

#### Phase 1 (data collecting script generation, using sample configuration in src/test/resources/ow2_projects.cfg):

```
java -jar target/data-collection.jar src/test/resources/ow2_projects.cfg 1
```
A /tmp/MRL directory should be created, with a script inside.

If MRL metadata endpoints are defined, MRL metadata files may also be present (name starting with MRL_).
Currently, only thresholds are supported as metadata (requires "MRL.thresholds" property pointing on MRL thresholds endpoint in ow2_projects.cfg), and collected in file "MRL_thresholds.csv".

#### Phase 2 (launch generated data collecting script):

```
/tmp/MRL/fetch_statistics.sh
```
The script should collect data for a few minutes, and save them as a bunch of .json files in /tmp/MRL.

#### Phase 3 (parse data and generate CSV file):

```
java -jar target/data-collection.jar src/test/resources/ow2_projects.cfg 3
```

Aggregated OMM data should be now available in CSV format, in file /tmp/MRL/metric_values.csv .

## Processing details

The package contains some sample data (those that can't be collected online, like OMM and scancode, supposed to be computed by batch), and a sample configuration file (ow2_projects.cfg), in src/test/resources .
The configuration file specifies:
- The list of OW2 projects to scan
- The output directory for data (default /tmp/MRL)
- For each scanned project, properties necessary to reach the project's Git repo, issue tracker, Sonarqube, scancode output, and OMM questionnaire data.
- Optionally, for a given project, overwrite value(s) for specified metric(s) (projectName.overwrite.metricName).

The data collection process is divided in 3 phases:
- Phase 1: generate a data collecting script for all projects (fetch_statistics.sh)
- Phase 2: run the data collecting script (can more efficiently be ran by hand, the script being executable).
- Phase 3: parse the data and generate an aggregated CSV file.

Each phase can be launched as follows:

```
java -jar target/data-collection.jar src/test/resources/ow2_projects.cfg <phase-number(s)>
```

Valid phase numbers are 1 (generate data collecting script), 2 (run script) or 3 (parse collected data).
Multiple phases can be chained, using comma-separated phase numbers (eg. 1,2,3).

# MRL Webapp

The MRL webapp (webapp/ directory) is packaged as a .war archive, and requires a MySQL database. 

## Initialize MySQL

Run the mysql_schema.sql SQL script located in src/main/resources (creates a OW2_MRL database if not exists, then populates it with table schemas and some data).

```
$ mysql -u username -p < src/main/resources/mysql_schema.sql
```

A test dataset is provided in src/test/resources/metric_values.csv .
It can be added to the OW2_MRL database as follows:

```
$ mysql -u username -p OW2_MRL -Bse "LOAD DATA LOCAL INFILE 'src/test/resources/metric_values.csv' INTO TABLE RawData FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n';"
```

### Insert real data and metadata

Real data (after data collection as described above) can be added to the OW2_MRL database as follows, supposing that data collection was issued in /tmp/MRL:

1) Insert metric values:

```
$ mysql -u username -p OW2_MRL -Bse "LOAD DATA LOCAL INFILE '/tmp/MRL/metric_values.csv' INTO TABLE RawData FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n';"
```

2) Insert MRL thresholds metadata (if any):

First, it is recommended to save the current Metric values (in case of failure):

```
mysql -u root -p OW2_MRL -Bse "DROP TABLE IF EXISTS SpareMetric;"
mysql -u root -p OW2_MRL -Bse "CREATE TABLE SpareMetric as select * from Metric;"
```

Then, to insert threshold values in Metric table:

```
mysql -u username -p OW2_MRL -Bse "LOAD DATA LOCAL INFILE '/tmp/MRL/MRL_thresholds.csv' REPLACE INTO TABLE Metric FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' (MetricName,Threshold1,Threshold2,Threshold3,Threshold4,Threshold5,Reverse);"
```

## Setup the webapp

Build the webapp

```
$ mvn clean install
```

Default database user/password are "root"/"password". If you use something else (recommended!), you will have to create a properties file with the following content (adapt user/password values according to your setup):

```
user: YOUR_MYSQL_USER_NAME
password: YOUR_MYSQL_PASSWORD
```

Webapp execution:

```
$ mvn jetty:run -Dmrl.conf.file.path=PATH_TO_YOUR_MYSQL_PROPERTIES_FILE
```

Note that if you used the default database user/password settings ("root"/"password") for testing, you can simply launch:

```
$ mvn jetty:run
```

The webapp is then available at http://localhost:8080/index.jsp .

## Automate data collection and database feeding

A collectdata.sh script is provided in data-collection/bin, as well as a sample congif in data-collection/conf/collection.conf.dist .

First edit collectdata.sh, to:
- Update MYSQL_USER, MYSQL_PASSWORD and MYSQL_DB
- Update MRL_ROOT (.. by default), if needed to run script from another directory or a crontab.
- Eventually change paths to configuration files or executable jars (supposed to be in $MRL_ROOT/data-collection)

Before *first* execution of the script, remove test data from MySQL, if any:
```
mysql> use OW2_MRL;
mysql> delete from RawData;
```

Run the script (can be done from a crontab):

```
$ ./collectdata.sh
```

Check the data is inserted, when no error occurs: using the webapp and/or the mysql shell.

### Add script to crontab (automation)

The script is intended to be run daily (at most), as data have a daily timestamp formatted as dd-MM-yyyy .
The date is included in the primary key, so if the script runs twice the same day, only the 1st data set will be included in the database (unless you delete it manually from MySQL).

To add it in the crontab:
- Make sure collectdata.sh is executable, and has MRL_ROOT set to an absolute path (so the cron will find commands).
- Edit the crontab to insert the script, for example using "crontab -e" (for user crontab) or editing /etc/crontab (system-wide).

Note: the following pattern would run the script every day a 4:30 AM:

```
30 4 * * * /path/to/mrl/collectdata.sh >> /tmp/mrl_collect.log
```
