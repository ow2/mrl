The OW2 Market Readiness Levels (MRL) methodology has been inspired by previous research and benefited from friendly contributions.

The following works were crucial in helping develop the methodology:
* On the RFC vocabulary: https://tools.ietf.org/html/rfc2119 
* On comparable evaluation methodologies: https://en.wikipedia.org/wiki/Open-source_software_assessment_methodologies 
* On maturity models: https://en.wikipedia.org/wiki/Maturity_model
* On the NASA TRL: http://www.spacewx.com/Docs/ISO_FDIS_16290_(E)_review.pdf
* On TRL in a business context: https://serkanbolat.com/2014/11/03/technology-readiness-level-trl-math-for-innovative-smes/
* On composite indicators: http://publications.jrc.ec.europa.eu/repository/bitstream/JRC31435/EUR%2022155%20EN.pdf (see appendix A)

The work on MRL was partially supported by two collaborative projects funded by the European Commission: 
* RISCOSS: https://riscoss.ow2.org/
* CROSSMINER: https://www.crossminer.org/ 

MRL also benefits from fruitful discussions with our friends at [Bitergia](https://bitergia.com/) specially [Valerio Cosentino](https://valeriocos.github.io/).

MRL source code was developed by:
* Pierre-Yves Gibello
* Antoine Mottier

Friendly contributions where provided by Stefano Zacchiroli and Vincent Massol who kindly reviewed the checklist of 50 best practices comprised in the first stage form to be completed by project leaders.
