<!--
SPDX-FileCopyrightText: 2021 OW2

SPDX-License-Identifier: Apache-2.0
-->

# MRL web app

## Deployment

### Prerequisites

- Java EE 7 application server with JSP and Servlet (version 3.1) support (e.g. Tomcat 8.5 or higher, Jetty 9.2 or higher).
- A MariaDB database initialize using [mysql_schema.sql](https://gitlab.ow2.org/ow2/mrl/-/blob/master/webapp/src/main/resources/mysql_schema.sql) SQL script.
- Credentials of a MariaDB user allow connecting to the database mentioned above and do create, read, update and delete operation and the database.
- Maven 3.6.3.
- JDK 11.

### Data collection deployment

TODO: explain usage of conf file templates and how to define their location

### Web App deployment

TODO: explain usage of conf file templates and how to define their location 

First step is to edit `frontend-webapp/src/main/resources/mysql.properties` file and specify the username and password.

Next you need to build the web application:
* Go to `webapp` folder.
* Run `mvn package`.
* Copy the file `mrl-webapp.war` to your application `webapps` folder and wait for automatic deployment to do its magic!
