<%--
SPDX-FileCopyrightText: 2021 OW2

SPDX-License-Identifier: Apache-2.0
--%>

<%@ page import="org.ow2.treewebapp.database.DatasetFactory" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html>

  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/ >

<!-- load the d3.js library --> 
<script src="https://d3js.org/d3.v3.min.js"></script>

    <style>
      body {
        font-family: sans-serif;
        font-size: 11px;
        font-weight: 300;
        fill: #242424;
        text-align: center;
        text-shadow: 0 1px 0 #fff, 1px 0 0 #fff, -1px 0 0 #fff, 0 -1px 0 #fff;
        cursor: default;
      }

      .legend {
        font-family: sans-serif;
        fill: #333333;
      }

      .tooltip {
        fill: #333333;
      }
    </style>

<%
String project = request.getParameter("project");
%>
<title><%=project%></title>
  </head>

  <body>

<%
if(request.getParameter("skipForm") == null) {
    List<String> projects = DatasetFactory.listProjects(23, "data missing");
    if(projects != null) {
        out.println("<form method=\"post\" action=\"radar.jsp\">");
        out.println("<select name=\"project\" onchange=\"this.form.submit()\">");
 
        for(String prj : projects) {
            String opval = prj;
            int pos = opval.indexOf("("); // eg. "docdoku (data missing)"
            if(pos > 2) opval = opval.substring(0, pos-1);

            out.println("<option value=\"" + opval + "\"" + (opval.equals(project) ? " selected" :"") + ">" + prj + "</option>");
        }
        out.println("</select>");
        out.println("</form>");

        if(project == null) project = projects.stream().findFirst().orElse(null);
    } else {
        out.println("<div>Error while listing projects names</div>");
    }
}
%>

    <script src="radarChart.js"></script>
    <script>
    
      /* Radar chart design created by Nadieh Bremer - VisualCinnamon.com */
    
      //////////////////////////////////////////////////////////////
      //////////////////////// Set-Up //////////////////////////////
      //////////////////////////////////////////////////////////////

      var margin = {top: 100, right: 100, bottom: 100, left: 100},
        width = Math.min(700, window.innerWidth - 10) - margin.left - margin.right,
        height = Math.min(width, window.innerHeight - margin.top - margin.bottom - 20);

      //////////////////////////////////////////////////////////////
      ////////////////////////// Data //////////////////////////////
      //////////////////////////////////////////////////////////////

      var data = [
    	  <%
    	  if(project != null) {
              DatasetFactory f = new DatasetFactory(project);
              out.println(f.getMRLTree().toRadarJson());
    	  }
    	  %>       
          ];
      //////////////////////////////////////////////////////////////
      //////////////////// Draw the Chart //////////////////////////
      //////////////////////////////////////////////////////////////

      var color = d3.scale.ordinal()
        .range(["#00A0B0", "#CC333F", "#EDC951"]);

      var radarChartOptions = {
        w: width,
        h: height,
        margin: margin,
        maxValue: 0.5,
        levels: 5,
        // Set roundStrokes to true makes radar lines curves
        roundStrokes: false,
        color: color
      };
      //Call function to draw the Radar chart
      RadarChart("body", data, radarChartOptions);
    </script>
  </body>

</html>
