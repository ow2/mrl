<%--
SPDX-FileCopyrightText: 2021 OW2

SPDX-License-Identifier: Apache-2.0
--%>

<%@ page import="org.ow2.treewebapp.database.DatasetFactory" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html>

  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/ >


<%
String project = request.getParameter("project");
%>
<title><%=project%></title>
</head>

<body>

<%
if(request.getParameter("skipForm") == null) {
    List<String> projects = DatasetFactory.listProjects(23, "data missing");
    if(projects != null) {
        out.println("<form method=\"post\" action=\"radar.jsp\">");
        out.println("<select name=\"project\" onchange=\"this.form.submit()\">");
 
        for(String prj : projects) {
            String opval = prj;
            int pos = opval.indexOf("("); // eg. "docdoku (data missing)"
            if(pos > 2) opval = opval.substring(0, pos-1);

            out.println("<option value=\"" + opval + "\"" + (opval.equals(project) ? " selected" :"") + ">" + prj + "</option>");
        }
        out.println("</select>");
        out.println("</form>");

        if(project == null) project = projects.stream().findFirst().orElse(null);
    } else {
        out.println("<div>Error while listing projects names</div>");
    }
}
%>

<%
   long score = 0;
   double rawScore = 0;
   if(project != null) {
     DatasetFactory f = new DatasetFactory(project);
     score = f.getOverallScore();
     rawScore = 2 * f.getNotRoundedOverallScore();
   }
   
   String readable[] = new String[] {
	"Research", "Development", "Fledgeling usefulness", "Demonstrated", "Usefulness",
	"Market opening", "Market broadening", "Business build up", "Actively competitive", "Established player" };

   out.println("<p><b>Raw score: " + rawScore + "</b></p>");
   out.println("<table>");
   for(int i = 10; i >= 1; i--) {
	   String bgcolor = (i == score ? "#ff0000" : "#ddd");
	   out.print("<tr bgcolor=\"" + bgcolor + "\"><td>");
	   out.print(i + " - " + readable[i-1]);
	   out.println("</td></tr>");
   }
   out.println("</table>");
%>

</body>
</html>
