<%--
SPDX-FileCopyrightText: 2021 OW2

SPDX-License-Identifier: Apache-2.0
--%>

<%@ page import="org.ow2.treewebapp.database.DatasetFactory" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
        <style>

 .node circle {
   fill: #fff;
   stroke: steelblue;
   stroke-width: 3px;
 }

 .node text { font: 12px sans-serif; }

 .link {
   fill: none;
   stroke: #ccc;
   stroke-width: 2px;
 }
 
    </style>

<%
String project = request.getParameter("project");
%>
<title><%=project%></title>
  </head>

  <body>

<%
if(request.getParameter("skipForm") == null) {
    List<String> projects = DatasetFactory.listProjects(23, "data missing");
    if(projects != null) {
        out.println("<form method=\"post\" action=\"dendrogram.jsp\">");
        out.println("<select name=\"project\" onchange=\"this.form.submit()\">");


        for(String prj : projects) {
            String opval = prj;
            int pos = opval.indexOf("("); // eg. "docdoku (data missing)"
            if(pos > 2) opval = opval.substring(0, pos-1);

            out.println("<option value=\"" + opval + "\"" + (opval.equals(project) ? " selected" :"") + ">" + prj + "</option>");
        }
        out.println("</select>");
        out.println("</form>");

        if(project == null) project = projects.stream().findFirst().orElse(null);
     } else {
        out.println("<div>Error while listing projects names</div>");
     }
}
%>

<!-- load the d3.js library --> 
<script src="https://d3js.org/d3.v3.min.js"></script>
 
<script>

var treeData = [
<%
if(project != null) {
    DatasetFactory f = new DatasetFactory(project);
    out.println(f.getMRLTree().toDendrogramJson());
}
%>
];

// ************** Generate the tree diagram  *****************
var margin = {top: 0, right: 120, bottom: 20, left: 120},
 width = 3000 - margin.right - margin.left,
 height = 1000 - margin.top - margin.bottom;
 
var i = 0;

var tree = d3.layout.tree()
 .size([height, width]);

var diagonal = d3.svg.diagonal()
 .projection(function(d) { return [d.y, d.x]; });

var svg = d3.select("body").append("svg")
 .attr("width", width + margin.right + margin.left)
 .attr("height", height + margin.top + margin.bottom)
  .append("g")
 .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

root = treeData[0];
  
update(root);

function update(source) {

  // Compute the new tree layout.
  var nodes = tree.nodes(root).reverse(),
   links = tree.links(nodes);

  // Normalize for fixed-depth.
  nodes.forEach(function(d) { d.y = d.depth * 250; });

  // Declare the nodes
  var node = svg.selectAll("g.node")
   .data(nodes, function(d) { return d.id || (d.id = ++i); });

  // Enter the nodes.
  var nodeEnter = node.enter().append("g")
   .attr("class", "node")
   .attr("transform", function(d) { 
    return "translate(" + d.y + "," + d.x + ")"; });

  nodeEnter.append("circle")
   .attr("r", 8)
   .style("fill", "#fff");

  nodeEnter.append("text")
   .attr("x", function(d) { 
    return d.children || d._children ? -13 : 13; })
   .attr("dy", ".35em")
   .attr("text-anchor", function(d) { 
    return d.children || d._children ? "end" : "start"; })
   .text(function(d) { return d.name + " - " + d.value; })
   .style("fill-opacity", 1);

  // Declare the links
  var link = svg.selectAll("path.link")
   .data(links, function(d) { return d.target.id; });

  // Enter the links.
  link.enter().insert("path", "g")
   .attr("class", "link")
   .attr("d", diagonal);

}

</script>
 
  </body>
</html>
