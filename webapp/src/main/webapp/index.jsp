<%--
SPDX-FileCopyrightText: 2021 OW2

SPDX-License-Identifier: Apache-2.0
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="org.ow2.treewebapp.database.DatasetFactory" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial;}

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border-top: none;
}
</style>

<%
String project = request.getParameter("project");
%>

<title>OW2 MRL<%=(project != null ? ": " + project : "")%></title>

</head>
<body>

<h2>OW2 MRL</h2>

<p>
<%
List<String> projects = DatasetFactory.listProjects(23, "data missing");
if(projects != null) {
%>
<form method="post" action="index.jsp">
Select project:
<select name="project" onchange="this.form.submit()">
<%
    for(String prj : projects) {
        String opval = prj;
        int pos = opval.indexOf("("); // eg. "docdoku (data missing)"
        if(pos > 2) opval = opval.substring(0, pos-1);
%>
	<option value="<%=opval%>" <%=(opval.equals(project) ? " selected" :"") %>><%=prj%></option>
<%
    }
%>
</select>
</form>
<%
if(project == null) project = projects.stream().findFirst().orElse(null);

} else {
%>
<div>Error while listing projects names</div>
<% } %>

</p>

<div class="tab">
  <button class="tablinks" onclick="openTab(event, 'Radar')">Radar view</button>
  <button class="tablinks" onclick="openTab(event, 'Dendrogram')">Tree view</button>
  <button class="tablinks" onclick="openTab(event, 'Score')">Score</button>
</div>

<div id="Radar" class="tabcontent">
<% if(project != null) { %>
 <object type="text/html" data="radar.jsp?skipForm=true&project=<%=project%>" width="1600px" height="1200px">
<% } else { %>
<object type="text/html" data="radar.jsp?skipForm=true" width="1600px" height="1200px">
<% } %>
    </object>
</div>

<div id="Dendrogram" class="tabcontent">
<% if(project != null) { %>
  <object type="text/html" data="dendrogram.jsp?skipForm=true&project=<%=project%>" width="1600px" height="1200px">
<% } else { %>
    <object type="text/html" data="dendrogram.jsp?skipForm=true>" width="1600px" height="1200px">
<% } %>
    </object>
</div>

<div id="Score" class="tabcontent">
<% if(project != null) { %>
 <object type="text/html" data="score.jsp?skipForm=true&project=<%=project%>" width="1600px" height="1200px">
<% } else { %>
<object type="text/html" data="score.jsp?skipForm=true" width="1600px" height="1200px">
<% } %>
    </object>
</div>

<script>
function openTab(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}
document.getElementsByClassName('tablinks')[0].click();
</script>

</body>

</html>
