/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.rest.attribute;

public enum RadarChartAttribute implements AttributeInterface {

    ACTIVITY(0, Constants.ACTIVITY_JSON_VALUE, "Activity"),
    COMMUNITY(1, Constants.COMMUNITY_JSON_VALUE, "Community"),
    COMPLIANCE(2, Constants.COMPLIANCE_JSON_VALUE, "Compliance"),
    QUALITY(3, Constants.QUALITY_JSON_VALUE, "Quality"),
    SUSTAINABILITY(4, Constants.SUSTAINABILITY_JSON_VALUE, "Sustainability");

    private final int index;
    private final String jsonName;
    private final String databaseName;

    RadarChartAttribute(int index, String jsonName, String databaseName) {
        this.index = index;
        this.jsonName = jsonName;
        this.databaseName = databaseName;
    }

    public int getIndex() {
        return index;
    }

    public String getJsonName() {
        return jsonName;
    }

    @Override
    public String getDatabaseName() {
        return databaseName;
    }

    public static class Constants {
        public static final String ACTIVITY_JSON_VALUE = "Activity";
        public static final String COMMUNITY_JSON_VALUE = "Community";
        public static final String COMPLIANCE_JSON_VALUE = "Compliance";
        public static final String QUALITY_JSON_VALUE = "Quality";
        public static final String SUSTAINABILITY_JSON_VALUE = "Sustainability";
    }
}
