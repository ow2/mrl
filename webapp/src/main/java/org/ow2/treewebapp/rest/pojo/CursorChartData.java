/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.rest.pojo;

public class CursorChartData {

    private final String[] names = {"Research",
            "Development",
            "Fledgeling Usefulness",
            "Usefulness Verified",
            "Fair Adoption or Market",
            "Sizable Adoption or Market",
            "Established Business",
            "Established Outsider",
            "Market Leader"
    };

    private double value;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String[] getNames() {
        return names;
    }
}
