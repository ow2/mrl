/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.rest.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

public class MRLNode {
    private String name;
    private String value;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<MRLNode> children;

    public MRLNode(String name, String value, List<MRLNode> children) {
        this.name = name;
        this.value = value;
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<MRLNode> getChildren() {
        return children;
    }

    public void setChildren(List<MRLNode> children) {
        this.children = children;
    }
}
