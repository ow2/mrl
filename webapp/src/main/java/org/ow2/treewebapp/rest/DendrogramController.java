/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.rest;

import org.ow2.treewebapp.rest.pojo.MRLNode;
import org.ow2.treewebapp.services.DendrogramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
public class DendrogramController {

    @Autowired
    private DendrogramService dendrogramService;

    @GetMapping("dendrogram/{name}")
    @CrossOrigin("*")
    List<MRLNode> nodes(@PathVariable String name) {
        return Collections.singletonList(dendrogramService.getDendrogram(name));
    }
}
