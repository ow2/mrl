/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.rest.pojo;

public class RadarChartDataLegacy {

    private RadarChartDataContainer radarChartContainer;

    public RadarChartDataContainer getRadarChartContainer() {
        return radarChartContainer;
    }

    public void setRadarChartContainer(RadarChartDataContainer radarChartContainer) {
        this.radarChartContainer = radarChartContainer;
    }
}
