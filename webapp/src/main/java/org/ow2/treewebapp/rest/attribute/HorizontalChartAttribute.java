/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.rest.attribute;

import static org.ow2.treewebapp.rest.attribute.HorizontalChartAttribute.Constants.*;

public enum HorizontalChartAttribute implements AttributeInterface {

    COMMUNICATION(COMMUNICATION_JSON_VALUE, "OMM_COM"),
    COMMUNITY(COMMUNITY_JSON_VALUE, "OMM_CTY"),
    DOCUMENTATION(DOCUMENTATION_JSON_VALUE, "OMM_DOC"),
    INFRASTRUCTURE(INFRASTRUCTURE_JSON_VALUE, "OMM_INF"),
    MANAGEMENT(MANAGEMENT_JSON_VALUE, "OMM_MGT"),
    LICENSES(LICENSES_JSON_VALUE, "OMM_LIC"),
    DEV_PROCESS(DEV_PROCESS_JSON_VALUE, "OMM_DEV"),
    TEST_PROCESS(TEST_PROCESS_JSON_VALUE, "OMM_TST"),
    RELEASE_MANAGEMENT(RELEASE_MANAGEMENT_JSON_VALUE, "OMM_REL"),
    SECURITY(SECURITY_JSON_VALUE, "OMM_SEC");

    private final String jsonName;

    private final String databaseName;

    HorizontalChartAttribute(String jsonName, String databaseName) {
        this.jsonName = jsonName;
        this.databaseName = databaseName;
    }

    public final String getJsonName() {
        return jsonName;
    }

    public final String getDatabaseName() {
        return databaseName;
    }

    public static class Constants {
        public static final String COMMUNICATION_JSON_VALUE = "Communication";
        public static final String COMMUNITY_JSON_VALUE = "Community";
        public static final String DOCUMENTATION_JSON_VALUE = "Documentation";
        public static final String INFRASTRUCTURE_JSON_VALUE = "Infrastructure";
        public static final String MANAGEMENT_JSON_VALUE = "Management";
        public static final String LICENSES_JSON_VALUE = "Licenses";
        public static final String DEV_PROCESS_JSON_VALUE = "Dev. Process";
        public static final String TEST_PROCESS_JSON_VALUE = "Test Process";
        public static final String RELEASE_MANAGEMENT_JSON_VALUE = "Release Mgt";
        public static final String SECURITY_JSON_VALUE = "Risk Mgt";
    }
}
