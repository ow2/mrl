/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.rest.pojo;

import com.fasterxml.jackson.annotation.JsonValue;

public class RadarChartDataContainer {
    @JsonValue
    RadarChartIndividualData[] radarChartIndividualData;

    public void setRadarChartIndividualData(RadarChartIndividualData[] radarChartIndividualData) {
        this.radarChartIndividualData = radarChartIndividualData;
    }

    public RadarChartIndividualData[] getRadarChartIndividualData() {
        return radarChartIndividualData;
    }
}
