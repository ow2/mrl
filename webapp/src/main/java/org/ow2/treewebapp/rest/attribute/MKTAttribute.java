/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.rest.attribute;

import java.util.Arrays;

public enum MKTAttribute {
    COMMUNITY("MKT_Community", "Developers and Community", "MKT-2", "Developers and Community"),
    CUSTOMERS("MKT_Customers", "User and Customer Base", "MKT-4", "User and Customer Base"),
    FINANCES("MKT_Finances", "Financial Stability", "MKT-6", "FInancial Stability"),
    MARKETING("MKT_Marketing", "Market Recognition", "MKT-7", "Market Recognition"),
    PRODUCT("MKT_Product", "Product Establishment", "MKT-1", "Product Establishment"),
    SALES("MKT_Sales", "Sales Activity", "MKT-5", "Sales Activity"),
    SUPPORT("MKT_Support", "Professional Support", "MKT-3", "Professional Support");

    /**
     * Name as used in the database
     */
    private final String databaseMetricName;
    /**
     * Name use when displaying the dendrogram (will be used when generating the JSON that is part of dendrogram API response)
     */
    private final String displayName;
    /**
     * Name as defined in https://projects.ow2.org/edit/ow2/MKTFormClass
     */
    private final String name;
    /**
     * Pretty name as defined in https://projects.ow2.org/edit/ow2/MKTFormClass
     */
    private final String prettyName;

    MKTAttribute(String databaseMetricName, String displayName, String name, String prettyName) {
        this.databaseMetricName = databaseMetricName;
        this.displayName = displayName;
        this.name = name;
        this.prettyName = prettyName;
    }

    public String getDatabaseMetricName() {
        return databaseMetricName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static String databaseMetricNameToDisplayName(String databaseMetricName) {
        for (MKTAttribute attribute : MKTAttribute.values()) {
            if (attribute.databaseMetricName.equals(databaseMetricName)) {
                return attribute.displayName;
            }
        }

        return null;
    }

    public static String[] databaseMetricNames() {
        return Arrays.stream(MKTAttribute.values()).map(MKTAttribute::getDatabaseMetricName).toArray(String[]::new);
    }
}
