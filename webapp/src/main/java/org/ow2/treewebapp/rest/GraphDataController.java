/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.rest;

import org.ow2.treewebapp.rest.pojo.GraphData;
import org.ow2.treewebapp.services.GraphDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GraphDataController {

    @Autowired
    private GraphDataService graphDataService;

    @GetMapping("graph-data/{name}")
    @CrossOrigin("*")
    GraphData one(@PathVariable String name) {
        return graphDataService.getGraphData(name);
    }
}
