/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.rest.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GraphData {
    private HorizontalChartData horizontalChartData;

    @JsonProperty("radarChartData")
    private RadarChartDataContainer[] radarChartDataContainers;

    private CursorChartData cursorChartData;

    public HorizontalChartData getHorizontalChartData() {
        return horizontalChartData;
    }

    public void setHorizontalChartData(HorizontalChartData horizontalChartData) {
        this.horizontalChartData = horizontalChartData;
    }

    public RadarChartDataContainer[] getRadarChartDataContainers() {
        return radarChartDataContainers;
    }

    public void setRadarChartDataContainers(RadarChartDataContainer[] radarChartDataContainers) {
        this.radarChartDataContainers = radarChartDataContainers;
    }

    public CursorChartData getCursorChartData() {
        return cursorChartData;
    }

    public void setCursorChartData(CursorChartData cursorChartData) {
        this.cursorChartData = cursorChartData;
    }
}
