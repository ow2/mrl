/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.rest.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.ow2.treewebapp.rest.attribute.HorizontalChartAttribute;

public class HorizontalChartData {

    @JsonProperty(HorizontalChartAttribute.Constants.COMMUNICATION_JSON_VALUE)
    private double communication;

    @JsonProperty(HorizontalChartAttribute.Constants.COMMUNITY_JSON_VALUE)
    private double community;

    @JsonProperty(HorizontalChartAttribute.Constants.DOCUMENTATION_JSON_VALUE)
    private double documentation;

    @JsonProperty(HorizontalChartAttribute.Constants.INFRASTRUCTURE_JSON_VALUE)
    private double infrastructure;

    @JsonProperty(HorizontalChartAttribute.Constants.MANAGEMENT_JSON_VALUE)
    private double management;

    @JsonProperty(HorizontalChartAttribute.Constants.LICENSES_JSON_VALUE)
    private double licenses;

    @JsonProperty(HorizontalChartAttribute.Constants.DEV_PROCESS_JSON_VALUE)
    private double devProcess;

    @JsonProperty(HorizontalChartAttribute.Constants.TEST_PROCESS_JSON_VALUE)
    private double testProcess;

    @JsonProperty(HorizontalChartAttribute.Constants.RELEASE_MANAGEMENT_JSON_VALUE)
    private double releaseManagement;

    @JsonProperty(HorizontalChartAttribute.Constants.SECURITY_JSON_VALUE)
    private double security;

    public double getDocumentation() {
        return documentation;
    }

    public void setDocumentation(double documentation) {
        this.documentation = documentation;
    }

    public double getInfrastructure() {
        return infrastructure;
    }

    public void setInfrastructure(double infrastructure) {
        this.infrastructure = infrastructure;
    }

    public double getManagement() {
        return management;
    }

    public void setManagement(double management) {
        this.management = management;
    }

    public double getLicenses() {
        return licenses;
    }

    public void setLicenses(double licenses) {
        this.licenses = licenses;
    }

    public double getDevProcess() {
        return devProcess;
    }

    public void setDevProcess(double devProcess) {
        this.devProcess = devProcess;
    }

    public double getTestProcess() {
        return testProcess;
    }

    public void setTestProcess(double testProcess) {
        this.testProcess = testProcess;
    }

    public double getReleaseManagement() {
        return releaseManagement;
    }

    public void setReleaseManagement(double releaseManagement) {
        this.releaseManagement = releaseManagement;
    }

    public double getSecurity() {
        return security;
    }

    public void setSecurity(double security) {
        this.security = security;
    }

    public double getCommunity() {
        return community;
    }

    public void setCommunity(double community) {
        this.community = community;
    }

    public double getCommunication() {
        return communication;
    }

    public void setCommunication(double communication) {
        this.communication = communication;
    }
}
