/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Metric extends TreeNode {

    String project;
    String description;
    String type;
    double threshold1, threshold2, threshold3, threshold4, threshold5;
    boolean reverse;
    String filter;

    /**
     * Initialize metric
     *
     * @param project Project name (null if none)
     * @param rs A ResultSet with at least Metric columns (may include a Value of type Double)
     * @throws SQLException error when running the query on the database
     */
    public Metric(String project, ResultSet rs) throws SQLException {
        super(rs.getString("MetricName"));
        this.project = project;
        try {
            setSensorValue(rs.getDouble("Value"));
        } catch (SQLException e) {
            setSensorValue(-1L);
        }
        this.description = rs.getString("Description");
        this.type = rs.getString("Type");
        this.reverse = rs.getBoolean("Reverse");
        this.threshold1 = rs.getDouble("Threshold1");
        this.threshold2 = rs.getDouble("Threshold2");
        this.threshold3 = rs.getDouble("Threshold3");
        this.threshold4 = rs.getDouble("Threshold4");
        this.threshold5 = rs.getDouble("Threshold5");

        if (getSensorValue() < 0) setValue(0); // Valid values are always positive
        else if (getSensorValue() < threshold1) setValue(reverse ? 5 : 0);
        else if (getSensorValue() < threshold2) setValue(reverse ? 4 : 1);
        else if (getSensorValue() < threshold3) setValue(reverse ? 3 : 2);
        else if (getSensorValue() < threshold4) setValue(reverse ? 2 : 3);
        else if (getSensorValue() < threshold5) setValue(reverse ? 1 : 4);
        else setValue(reverse ? 0 : 5);
    }

    public Metric(String project, String metricName, double value) {
        super(metricName);
        this.project = project;
        this.project = "";
        this.type = "last";
        this.reverse = false;

        setSensorValue(value);
    }

    public String getProject() {
        return project;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getThreshold1() {
        return threshold1;
    }

    public void setThreshold1(double threshold1) {
        this.threshold1 = threshold1;
    }

    public double getThreshold2() {
        return threshold2;
    }

    public void setThreshold2(double threshold2) {
        this.threshold2 = threshold2;
    }

    public double getThreshold3() {
        return threshold3;
    }

    public void setThreshold3(double threshold3) {
        this.threshold3 = threshold3;
    }

    public double getThreshold4() {
        return threshold4;
    }

    public void setThreshold4(double threshold4) {
        this.threshold4 = threshold4;
    }

    public double getThreshold5() {
        return threshold5;
    }

    public void setThreshold5(double threshold5) {
        this.threshold5 = threshold5;
    }

    public boolean isReverse() {
        return reverse;
    }

    public void setReverse(boolean reverse) {
        this.reverse = reverse;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    /**
     * Update metric (Metric table). Only Metric table fields are updated
     * (eg. project and value are out of scope).
     *
     * @param conn             database connection.
     * @param updateThresholds If true, update thresholds + reverse.
     */
    public void updateMetric(Connection conn, boolean updateThresholds) throws SQLException {
        StringBuilder sql = new StringBuilder("update Metric set ");
        boolean update = false;
        if (description != null) {
            sql.append("Description='").append(description).append("'");
            update = true;
        }
        if (type != null) {
            sql.append(update ? "," : "").append("Type='").append(type).append("'");
            update = true;
        }
        if (updateThresholds) {
            sql.append(update ? "," : "")
		.append("Threshold1=").append(threshold1)
		.append(",Threshold2=").append(threshold2)
		.append(",Threshold3=").append(threshold3)
		.append(",Threshold4=").append(threshold4)
		.append(",Threshold5=").append(threshold5)
		.append(",Reverse=").append(reverse ? 1 : 0);
            update = true;
        }
        if (filter != null) {
            sql.append(update ? "," : "").append("Filter='").append(filter).append("'");
            update = true;
        }
        sql.append(" where MetricName='").append(name).append("'");

        if(update) conn.createStatement().executeUpdate(sql.toString());
    }

}

