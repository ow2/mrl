/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.database;

import org.ow2.treewebapp.rest.attribute.MKTAttribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class DatasetFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(DatasetFactory.class);
    // Database credentials
    static String databaseHost = "localhost", database = "OW2_MRL", user = "root", password = "password";

    /** Name of the JVM system property that define the location of the configuration file.
     * Configuration file define settings to connect to MySQL/MariaDB database */
    public static final String MRL_CONF_FILE_PATH = "mrl.conf.file.path";

    static {
        try {
            Class.forName("org.mariadb.jdbc.Driver");

            try {
                String configurationFilePath = System.getProperty(MRL_CONF_FILE_PATH);
                LOGGER.debug("Loading database connection configuration from: {}", configurationFilePath);
                InputStream databaseConfigurationFile = Files.newInputStream(Paths.get(configurationFilePath));
                Properties configuration = new Properties();
                configuration.load(databaseConfigurationFile);
                LOGGER.debug("Database connection configuration loaded: {}", configuration.toString());
                String credential = configuration.getProperty("databaseHost");
                if (credential != null) DatasetFactory.databaseHost = credential.trim();
                credential = configuration.getProperty("database");
                if (credential != null) DatasetFactory.database = credential.trim();
                credential = configuration.getProperty("user");
                if (credential != null) DatasetFactory.user = credential.trim();
                credential = configuration.getProperty("password");
                if (credential != null) DatasetFactory.password = credential.trim();
            } catch (IOException e) {
                LOGGER.error("Not able to load database connection configuration", e);
            }
        } catch (Exception e) {
            LOGGER.error("Error in MariaDB configuration reading", e);
        }
    }

    final HashMap<String, Metric> metrics = new HashMap<>();
    final String project;

    public DatasetFactory(String project) {
        this.project = project;
    }

    /**
     * List projects with data in database, for latest collection date
     *
     * @param minData   Minimum data count for project
     * @param emphasize If null, remove data under count, else emphasize it
     * @return list of projects or null if error occurs
     */
    public static List<String> listProjects(int minData, String emphasize) {
        LinkedList<String> projects = new LinkedList<>();
        Connection conn = null;
        try {
            conn = DatasetFactory.connectDB();

            // Get project list
            ResultSet rs = conn.createStatement().executeQuery(
                    "select Project, count(Project) as cnt from RawData where Date="
                            + "(select max(Date) from RawData)"
                            + " group by Project order by cnt desc, Project");
            while (rs.next()) {
                int ndata = rs.getInt("cnt");
                if (ndata < minData) {
                    if (emphasize != null) projects.add(rs.getString("Project") + " (" + emphasize + ")");
                } else {
                    projects.add(rs.getString("Project"));
                }
            }
            rs.close();

        } catch (SQLException ex) {
            // handle any errors
            LOGGER.error("Error while listing the projects names. SQLException: {} SQLState: {} VendorError: {}", ex.getMessage(), ex.getSQLState(), ex.getErrorCode());

            return null;
        } finally {
            if (conn != null) try {
                conn.close();
            } catch (Exception ignore) {
            }
        }
        return projects;
    }

    private static Connection connectDB() throws SQLException {
        return DriverManager.getConnection("jdbc:mariadb://" + DatasetFactory.databaseHost + "/"
                + DatasetFactory.database
                + "?user=" + DatasetFactory.user
                + "&password=" + DatasetFactory.password);
    }

    /**
     * Overall score is a score out of 10, calculated as follows:
     * Round (2 x (mean of all MRL attributes + MKT capabilities))
     * For the record, MRL / MKT scores are scores out of 5...
     *
     * @return The overall score (out of 10, rounded)
     */
    public long getOverallScore() {
        // Multiply by 2, as average is out of 5, and score out of 10
        // Truncate to get an int value (e.g. 9. becomes 9)
        // Exclude value 0 and 10
        return Math.max(1L, Math.min(9L, Math.round(2D * getNotRoundedOverallScore())));
    }

    /**
     * Get raw (not rounded) overall score.
     * @return The score (out of 5).
     */
    public double getNotRoundedOverallScore() {
        int count = 0;
        double total = 0D;

        // Extract data from MRL tree
        // This must be done FIRST because it fills this.metrics table !
        TreeNode mrl = getMRLTree();
        if (mrl.getChildren() != null) {
            for (TreeNode node : mrl.getChildren()) {
                total += node.computeValue();
                count++;
            }
        }

        for (MKTAttribute attribute : MKTAttribute.values()) {
            Metric mkt = this.metrics.get(attribute.getDatabaseMetricName());
            if (mkt != null) {
                total += mkt.getSensorValue();
                count++;
            }
        }

        if (total <= 0D) total = 0D;

        return total / count;
    }

    /**
     * Retrieves MRL tree
     *
     * @return The MRL tree
     */
    public TreeNode getMRLTree() {

        Connection conn = null;
        try {
            conn = DatasetFactory.connectDB();

            // Get all metric data for project
            ResultSet rs = conn.createStatement().executeQuery(
                    "select m.MetricName as MetricName, Value, Type, Filter, Description, Threshold1, Threshold2, Threshold3, Threshold4, Threshold5, Reverse"
                            + " from RawData d, Metric m"
                            + " where m.MetricName = d.MetricName and Project='" + this.project + "'"
                            + " order by date desc");

            metrics.clear();
            while (rs.next()) {
                Metric m = new Metric(this.project, rs);
                //TODO Only latest metric value taken into account: ok for last, what about average is any ?
                metrics.putIfAbsent(m.getName(), m);
            }
            rs.close();

            return buildTree(this.project);

        } catch (SQLException ex) {
            // handle any errors
            LOGGER.error("Error while getting MRL tree for project: {}. SQLException: {} SQLState: {} VendorError: {}", this.project, ex.getMessage(), ex.getSQLState(), ex.getErrorCode());
            return null;
        } finally {
            if (conn != null) try {
                conn.close();
            } catch (Exception ignore) {
            }
        }
    }

    public void fetchMKTMetrics() {
        try {
            Connection connection = DatasetFactory.connectDB();
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery("SELECT * FROM (SELECT Project, Date, MetricName, Value, row_number() over(partition by MetricName order by Date desc) AS rn " +
                        "FROM RawData WHERE Project = '" + project + "' AND MetricName LIKE 'MKT_%') t WHERE t.rn = 1;");
                while (resultSet.next()) {
                    Metric m = new Metric(project, resultSet.getString("MetricName"), resultSet.getDouble("Value"));
                    metrics.putIfAbsent(m.getName(), m);
                }
            } catch (SQLException e) {
                LOGGER.error("Error while trying to get MKT metrics", e);
            }
        } catch (SQLException e) {
            LOGGER.error("Error when connecting to the database", e);
        }
    }

    /**
     * Retrieves a node given its name
     *
     * @param root (sub) tree to search
     * @param name Node name
     * @return The requested node, null if not found
     */
    public TreeNode findNodeByName(TreeNode root, String name) {
        if (root.getName().contentEquals(name)) return root;
        else {
            List<TreeNode> children = root.getChildren();
            if (children != null) {
                for (TreeNode node : children) {
                    TreeNode result = findNodeByName(node, name);
                    if (result != null) return result;
                }
            }
        }
        return null;
    }

    private TreeNode buildTree(String project) {
        TreeNode tree = new TreeNode(project);

        Connection conn = null;
        try {
            conn = DatasetFactory.connectDB();

            // Get all metric data for project
            ResultSet rs = conn.createStatement().executeQuery(
                    "select AttributeName, a.AggregateName as AggregateName, MetricName"
                            + " from Aggregate a, AggregateMetric m"
                            + " where m.aggregatename = a.aggregatename"
                            + " order by attributename, a.aggregatename");

            while (rs.next()) {
                tree.addNodes(rs.getString("AttributeName"), rs.getString("AggregateName"), this.metrics.get(rs.getString("MetricName")));
            }
            rs.close();

        } catch (SQLException ex) {
            // handle any errors
            LOGGER.error("Error while building the tree for project: {}. SQLException: {} SQLState: {} VendorError: {}", project, ex.getMessage(), ex.getSQLState(), ex.getErrorCode());
            return null;
        } finally {
            if (conn != null) try {
                conn.close();
            } catch (Exception ignore) {
            }
        }
        return tree;
    }

    public String getProject() {
        return project;
    }

    public HashMap<String, Metric> getMetrics() {
        return metrics;
    }
}
