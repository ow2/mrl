/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MetricMetadata {

	String name;
	String description;
	String type;
	String filter;
	double threshold1, threshold2, threshold3, threshold4, threshold5;
	boolean reverse;
	
	public MetricMetadata(String project, ResultSet rs) throws SQLException {
		this.name = rs.getString("MetricName");
		this.description = rs.getString("Description");
		this.type = rs.getString("Type");
		this.filter = rs.getString("Filter");
		this.threshold1 = rs.getDouble("Threshold1");
		this.threshold2 = rs.getDouble("Threshold2");
		this.threshold3 = rs.getDouble("Threshold3");
		this.threshold4 = rs.getDouble("Threshold4");
		this.threshold5 = rs.getDouble("Threshold4");
		this.reverse = rs.getBoolean("Reverse");
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getType() {
		return type;
	}

	public String getFilter() {
		return filter;
	}

	public double getThreshold1() {
		return threshold1;
	}

	public double getThreshold2() {
		return threshold2;
	}

	public double getThreshold3() {
		return threshold3;
	}

	public double getThreshold4() {
		return threshold4;
	}

	public double getThreshold5() {
		return threshold5;
	}
	
	public boolean getReverse() {
		return reverse;
	}
}

