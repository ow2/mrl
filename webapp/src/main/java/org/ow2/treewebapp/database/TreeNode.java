/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.database;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class TreeNode {
	
	final String name;
	TreeNode parent;
	double value;
	double sensorValue = -1L;
	List<TreeNode> children;
	
	public TreeNode(String name) {
		this.name = name;
	}
	
	public TreeNode addChild(TreeNode child) {
		if(this.children == null) this.children = new LinkedList<>();
		if(! this.children.contains(child)) {
			child.setParent(this);
			this.children.add(child);
		}
		return this.children.get(this.children.indexOf(child));
	}

	public void addNodes(String goal, String attribute, Metric metric) {
		if(goal != null) {
			TreeNode node = this.addChild(new TreeNode(goal));
			if(attribute != null) {
				node = node.addChild(new TreeNode(attribute));
				if(metric != null) {
					node.addChild(metric);
				}
			}
		}
	}

	public String getName() {
		return this.name;
	}
	
	public double getValue() {
		return this.value;
	}
	public void setValue(double value) {
		this.value = value;
	}

	public double getSensorValue() {
		return this.sensorValue;
	}
	public void setSensorValue(double sensorValue) {
		this.sensorValue = sensorValue;
	}

	public List<TreeNode> getChildren() {
		return children;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return this.getName().equals(((TreeNode)o).getName());
	}
	
	public boolean isRoot() {
		return this.parent == null;
	}
	
	public TreeNode getRoot() {
		TreeNode root = this;
		while(root != null && ! root.isRoot()) root = root.parent;
		return root;
	}

	public String toString() {
		return toDendrogramJson();
	}
	
	/**
	 * Generates JSON data for D3.js dendrogram
	 * @return JSON formatted for D3.js dendrogram
	 */
	public String toDendrogramJson() {
		return toDendrogramJson(0);
	}
	private String toDendrogramJson(int tab) {
		DecimalFormat df = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US));
		StringBuilder json = new StringBuilder();
		if(children == null) {
			tab(json, tab).append("{ \"name\": \"").append(this.getName()).append("\", \"parent\": \"").append(this.parent == null ? "" : this.parent.getName()).append("\", \"value\": \"").append(df.format(this.computeValue())).append(" (").append(this.sensorValue).append(")").append("\" }");
		} else {
			tab(json, tab).append("{ \"name\": \"").append(this.getName()).append("\", \"parent\": \"").append(this.parent == null ? "" : this.parent.getName()).append("\", \"value\": \"").append(df.format(this.computeValue())).append("\", \"children\": [\n");
			boolean first = true;
			for (TreeNode node : children) {
				tab(json, tab).append(first ? "" : ", ").append(node.toDendrogramJson(tab + 1));
				first = false;
			}
			json.append("\n");
			tab(json, tab).append("] }");
		}
		return json.toString();
	}
	
	/**
	 * Generates JSON data for D3.js radar display
	 * (like this one: https://github.com/alangrafu/radar-chart-d3)
	 * @return JSON formatted for D3.js radar
	 */
	public String toRadarJson() {
		DecimalFormat df = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US));
		StringBuilder json = new StringBuilder("[\n");
		List<TreeNode> attributes = getRoot().getChildren();
		for(TreeNode attribute : attributes) {
			json.append(" { \"axis\": \"").append(attribute.getName()).append("\", \"value\": \"").append(df.format(attribute.computeValue())).append("\" },\n");
		}
		json.append("]");
		return json.toString();
	}

	protected void setParent(TreeNode parent) {
		this.parent = parent;
	}

	/**
	 * Computes node value
	 * @return Average of children values, or node value for leafs
	 */
	public double computeValue() {
		double ret = this.value;
		if(children != null) {
			for (TreeNode child : children) {
				ret += child.computeValue();
			}
			ret /= children.size();
		}
		return ret;
	}

	/**
	 * Appends tabulation (spaces) to Stringbuilder
	 * @param b Target Stringbuilder
	 * @param tab Number of spaces to append
	 * @return the target string builder with spaces append to it
	 */
	private StringBuilder tab(StringBuilder b, int tab) {
		b.append(" ".repeat(Math.max(0, tab)));
		return b;
	}
}
