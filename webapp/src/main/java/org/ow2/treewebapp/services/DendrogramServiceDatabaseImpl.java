/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.services;

import org.ow2.treewebapp.database.DatasetFactory;
import org.ow2.treewebapp.database.Metric;
import org.ow2.treewebapp.database.TreeNode;
import org.ow2.treewebapp.rest.attribute.MKTAttribute;
import org.ow2.treewebapp.rest.pojo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DendrogramServiceDatabaseImpl implements DendrogramService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DendrogramServiceDatabaseImpl.class);

    private static final NumberFormat FORMAT = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US));


    @Override
    public MRLNode getDendrogram(String projectName) {

        DatasetFactory f = new DatasetFactory(projectName);
        TreeNode treeRootNode = f.getMRLTree();

        // Transform the tree provided by DatasetFactory into a tree using object defined / annotated for JSON serialization using Jackson library
        MRLNode rootNode = transform(treeRootNode);

        // Update the name and value (x2) of the root node
        rootNode.setName("MRL");
        rootNode.setValue(FORMAT.format(f.getNotRoundedOverallScore()*2D));

        // Add MKT node as direct child of the root
        addMKTNodes(f, rootNode);

        return rootNode;
    }

    private void addMKTNodes(DatasetFactory f, MRLNode rootNode) {
        f.fetchMKTMetrics();

        // Iterate over all known MKT attributes
        for (MKTAttribute attribute : MKTAttribute.values()) {
            // Get the attribute value using metrics map available in DatasetFactory (initialized with the call to getMRLTree)
            Metric mkt = f.getMetrics().get(attribute.getDatabaseMetricName());
            if (mkt != null) {
                // Create the node using the name defined in the enum and the value stored in the metrics map Metric object
                MRLNode node = new MRLNode(attribute.getDisplayName(), FORMAT.format(mkt.getSensorValue()), null);
                rootNode.getChildren().add(node);
                LOGGER.debug("Adding node: {} to the root node", node.getName());
            } else {
                LOGGER.warn("No metric found with name: {} (display name: {})", attribute.getDatabaseMetricName(), attribute.getDisplayName());
                if(LOGGER.isDebugEnabled()) {
                    for (Map.Entry<String, Metric> metricEntry : f.getMetrics().entrySet()) {
                        LOGGER.debug("Name in map: {} Name in Metric object: {} Value: {}", metricEntry.getKey(), metricEntry.getValue().getName(), metricEntry.getValue().getValue());
                    }
                }
            }
        }
    }

    private static MRLNode transform(TreeNode treeNode) {
        List<MRLNode> mrlNodes = null;
        if((treeNode.getChildren() != null) && (!treeNode.getChildren().isEmpty())) {
            mrlNodes = treeNode.getChildren().stream().map(DendrogramServiceDatabaseImpl::transform).collect(Collectors.toList());
        }
        LOGGER.debug("Transforming: {} with values: {} and formatted value: {}", treeNode.getName(), treeNode.getValue(), FORMAT.format(treeNode.getValue()));
        return new MRLNode(treeNode.getName(), FORMAT.format(treeNode.computeValue()), mrlNodes);
    }
}
