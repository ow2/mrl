/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.services;

import org.ow2.treewebapp.database.DatasetFactory;
import org.ow2.treewebapp.database.TreeNode;
import org.ow2.treewebapp.rest.attribute.AttributeInterface;
import org.ow2.treewebapp.rest.attribute.HorizontalChartAttribute;
import org.ow2.treewebapp.rest.attribute.RadarChartAttribute;
import org.ow2.treewebapp.rest.pojo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class GraphDataServiceDatabaseImpl implements GraphDataService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GraphDataServiceDatabaseImpl.class);

    @Override
    public GraphData getGraphData(String projectName) {
        return new GraphDataGenerator(new DatasetFactory(projectName)).generate();
    }


    /*private static double extract(List<TreeNode> nodes, HorizontalChartAttributes horizontalChartAttributes) {
        for (TreeNode node : nodes) {
            if(node.getName().equals(horizontalChartAttributes.getDatabaseName())) {
                return node.getValue();
            }
        }

        LOGGER.warn("No node match given name: " + horizontalChartAttributes.getJsonName());

        return 0.0;
    }*/

    private static class GraphDataGenerator {
        final DatasetFactory datasetFactory;

        GraphDataGenerator(DatasetFactory datasetFactory) {
            this.datasetFactory = datasetFactory;
        }

        public GraphData generate() {
            TreeNode mrlTree = datasetFactory.getMRLTree();
            LOGGER.debug("MRL tree for {} project:\n {}", datasetFactory.getProject(), mrlTree);

            GraphData graphData = new GraphData();

            HorizontalChartData horizontalChartData = new HorizontalChartData();
            graphData.setHorizontalChartData(horizontalChartData);

            horizontalChartData.setCommunication(extractNodeInformation(HorizontalChartAttribute.COMMUNICATION));
            horizontalChartData.setCommunity(extractNodeInformation(HorizontalChartAttribute.COMMUNITY));
            horizontalChartData.setDocumentation(extractNodeInformation(HorizontalChartAttribute.DOCUMENTATION));
            horizontalChartData.setInfrastructure(extractNodeInformation(HorizontalChartAttribute.INFRASTRUCTURE));
            horizontalChartData.setManagement(extractNodeInformation(HorizontalChartAttribute.MANAGEMENT));
            horizontalChartData.setLicenses(extractNodeInformation(HorizontalChartAttribute.LICENSES));
            horizontalChartData.setDevProcess(extractNodeInformation(HorizontalChartAttribute.DEV_PROCESS));
            horizontalChartData.setTestProcess(extractNodeInformation(HorizontalChartAttribute.TEST_PROCESS));
            horizontalChartData.setReleaseManagement(extractNodeInformation(HorizontalChartAttribute.RELEASE_MANAGEMENT));
            horizontalChartData.setSecurity(extractNodeInformation(HorizontalChartAttribute.SECURITY));

            RadarChartDataContainer radarChartDataContainer = new RadarChartDataContainer();
            RadarChartDataContainer[] radarChartDataContainerArray = {radarChartDataContainer};
            graphData.setRadarChartDataContainers(radarChartDataContainerArray);

            radarChartDataContainer.setRadarChartIndividualData(prepareRadarChartIndividualInformation());

            //RadarChartDataLegacy radarChartDataLegacy = new RadarChartDataLegacy();
            //radarChartDataLegacy.setRadarChartContainer(radarChartDataContainer);

            CursorChartData cursorChartData = new CursorChartData();
            graphData.setCursorChartData(cursorChartData);

            cursorChartData.setValue(datasetFactory.getOverallScore());

            return graphData;
        }

        private double extractNodeInformation(AttributeInterface attribute) {
            return datasetFactory.findNodeByName(datasetFactory.getMRLTree(), attribute.getDatabaseName()).computeValue();
        }

        private RadarChartIndividualData[] prepareRadarChartIndividualInformation() {

            return Arrays.stream(RadarChartAttribute.values()).map(attribute -> new RadarChartIndividualData(attribute.getJsonName(), extractNodeInformation(attribute))).toArray(RadarChartIndividualData[]::new);

        }

    }

}
