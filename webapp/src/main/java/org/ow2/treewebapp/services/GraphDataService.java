/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.treewebapp.services;

import org.ow2.treewebapp.rest.pojo.GraphData;

public interface GraphDataService {
    GraphData getGraphData(String projectName);
}
