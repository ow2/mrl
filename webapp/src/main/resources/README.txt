MySQL initialization:
- src/main/resources/mysql_schema.sql to create and initialize tables with MRL model.
- src/test/resources/metric_values.csv as sample data.

To insert sample data in RawData table:
mysql> LOAD DATA LOCAL INFILE 'metric_values.csv' INTO TABLE RawData FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n';

