CREATE DATABASE IF NOT EXISTS `OW2_MRL`;

USE `OW2_MRL`;

--
-- Table structure for table `Aggregate`
--

DROP TABLE IF EXISTS `Aggregate`;
CREATE TABLE `Aggregate` (
  `AggregateName` varchar(128) PRIMARY KEY,
  `AttributeName` varchar(128) references Attribute(AttributeName),
  `Description` varchar(1024) DEFAULT NULL
);

--
-- Dumping data for table `Aggregate`
--

LOCK TABLES `Aggregate` WRITE;
INSERT INTO `Aggregate` VALUES
('act_liveliness','Activity',NULL),
('act_bugs','Activity',NULL),
('act_reactivity','Activity',NULL),
('cty_omm','Community',NULL),
('cty_activeusers','Community',NULL),
('cty_unbugs','Community',NULL),
('lic_omm','Compliance',NULL),
('lic_uniquelicense','Compliance',NULL),
('lic_ratio','Compliance',NULL),
('qua_omm','Quality',NULL),
('qua_tests','Quality',NULL),
('qua_unbugs','Quality',NULL),
('qua_issues','Quality',NULL),
('sus_omm','Sustainability',NULL),
('sus_activeusers','Sustainability',NULL),
('sus_bugopen','Sustainability',NULL);
UNLOCK TABLES;

--
-- Table structure for table `AggregateMetric`
--

DROP TABLE IF EXISTS `AggregateMetric`;
CREATE TABLE `AggregateMetric` (
  `AggregateName` varchar(128) NOT NULL,
  `MetricName` varchar(128) NOT NULL,
  PRIMARY KEY (`AggregateName`,`MetricName`)
);

--
-- Dumping data for table `AggregateMetric`
--

LOCK TABLES `AggregateMetric` WRITE;
INSERT INTO `AggregateMetric` VALUES
('cty_omm', 'OMM_CTY'),
('cty_omm', 'OMM_INF'),
('cty_omm', 'OMM_MGT'),
('cty_omm', 'OMM_DOC'),
('cty_activeusers', 'ActiveUsers'),
('cty_unbugs', 'UnansweredBugs'),
('act_liveliness', 'OMM_COM'),
('act_liveliness', 'OMM_CTY'),
('act_liveliness', 'ActiveUsers'),
('act_bugs', 'Opentime'),
('act_bugs', 'UnansweredBugs'),
('act_reactivity', 'AverageResponseTime'),
('qua_omm','OMM_DOC'),
('qua_omm','OMM_INF'),
('qua_omm','OMM_DEV'),
('qua_omm','OMM_TST'),
('qua_omm','OMM_REL'),
('qua_omm','OMM_SEC'),
('qua_tests','QualitySonarQTestCoverage'),
('qua_tests','QualitySonarQTestSuccess'),
('qua_unbugs','UnansweredBugs'),
('qua_issues','BlockerIssue'),
('qua_issues','CriticalIssue'),
('lic_omm', 'OMM_LIC'),
('lic_uniquelicense', 'ScancodeUniqueLic'),
('lic_ratio', 'ScancodeRatioNoLic'),
('sus_omm', 'OMM_COM'),
('sus_omm', 'OMM_CTY'),
('sus_omm', 'OMM_DOC'),
('sus_omm', 'OMM_MGT'),
('sus_omm', 'OMM_REL'),
('sus_omm', 'OMM_SEC'),
('sus_activeusers', 'ActiveUsers'),
('sus_bugopen', 'Opentime');
UNLOCK TABLES;

--
-- Table structure for table `Attribute`
--

DROP TABLE IF EXISTS `Attribute`;
CREATE TABLE `Attribute` (
  `AttributeName` varchar(128) PRIMARY KEY,
  `Description` varchar(1024) DEFAULT NULL
);

--
-- Dumping data for table `Attribute`
--

LOCK TABLES `Attribute` WRITE;
INSERT INTO `Attribute` VALUES ('Activity',NULL),('Community',NULL),('Compliance',NULL),('Quality',NULL),('Sustainability',NULL);
UNLOCK TABLES;

--
-- Table structure for table `Metric`
--

DROP TABLE IF EXISTS `Metric`;
CREATE TABLE `Metric` (
  `MetricName` varchar(128) PRIMARY KEY,
  `Description` varchar(1024) DEFAULT NULL,
  `Type` enum('avg','last','max') DEFAULT 'last',
  `Threshold1` double DEFAULT '0.9',
  `Threshold2` double DEFAULT '1.9',
  `Threshold3` double DEFAULT '2.9',
  `Threshold4` double DEFAULT '3.9',
  `Threshold5` double DEFAULT '4.9',
  `Reverse` tinyint(1) DEFAULT '0',
  `Filter` varchar(1024) DEFAULT NULL
);

--
-- Dumping data for table `Metric`
--

LOCK TABLES `Metric` WRITE;
INSERT INTO `Metric` VALUES
('ActiveUsers','Bug Tracker Active Users','last',1,2,5,10,20,0,NULL),
('AverageResponseTime','Bug Average Response Time (Cumulative) (s)','avg',518400,2592000,7776000,15552000,31536000,1,NULL),
('BlockerIssue','Sonar blocker_violations','last',5,20,50,100,200,1,NULL),
('CriticalIssue','Sonar critical_violations','last',50,100,300,500,1000,1,NULL),
('LOCOverFilesOverTime','LOC over files over time','avg',0.9,0.85,0.8,0.7,0.6,1,NULL),
('MKT_Community',NULL,'last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('MKT_Customers',NULL,'last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('MKT_Finances',NULL,'last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('MKT_Marketing',NULL,'last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('MKT_Product',NULL,'last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('MKT_Sales',NULL,'last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('MKT_Support',NULL,'last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('OMM_COM','OMM_COM','last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('OMM_CTY','OMM_CTY','last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('OMM_DEV','OMM_DEV','last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('OMM_DOC','OMM_DOC','last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('OMM_INF','OMM_INF','last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('OMM_MGT','OMM_MGT','last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('OMM_LIC','OMM_LIC','last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('OMM_REL','OMM_REL','last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('OMM_SEC','OMM_SEC','last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('OMM_TST','OMM_TST','last',0.9,1.9,2.9,3.9,4.9,0,NULL),
('Opentime','Bug Open Time (days)','last',40,90,150,180,365,1,NULL),
('QualitySonarQTestCoverage','Sonar coverage','last',20,35,50,65,80,0,NULL),
('QualitySonarQTestSuccess','Sonar test_success_density','last',20,35,50,65,80,0,NULL),
('ScancodeRatioNoLic','scancode_ratio_no_lic','last',0.15,0.4,0.6,0.75,0.9,1,NULL),
('ScancodeUniqueLic','scancode_unique_lic','last',3,6,15,30,60,1,NULL),
('UnansweredBugs','Unanswered Bugs','last',15,50,100,150,300,1,NULL);
UNLOCK TABLES;

--
-- Table structure for table `RawData`
--

DROP TABLE IF EXISTS `RawData`;
CREATE TABLE `RawData` (
  `Project` varchar(128) NOT NULL,
  `Date` date NOT NULL,
  `MetricName` varchar(128) NOT NULL,
  `Value` double DEFAULT '-1',
  PRIMARY KEY(`Project`,`Date`,`MetricName`)
);

