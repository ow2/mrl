#!/bin/bash
echo "Executing ${0}"
set -eou pipefail

# SPDX-FileCopyrightText: 2021 OW2
#
# SPDX-License-Identifier: Apache-2.0

##
## Here you can customize your variables !
##
# Jetty user/group name
: "${WEBSERVER_USER:=jetty}"
: "${WEBSERVER_GROUP:=jetty}"

# MRL user/group name
: "${MRL_USER:=mrl}"
: "${MRL_GROUP:=ow2-tech}"

# MRL run directory
: "${MRL_RUN_DIR:=/srv/apprun/mrl}"
: "${MRL_WEBAPP_DEPLOYMENT_DIR:=/srv/appdata/jetty/base-mrl/webapps}"

##
## Do not edit anything below except if you know what you're doing !
##
GITLAB_HOST='gitlab.ow2.org'

# MRL GitLab project id
PROJECT_ID=1358

# WebApp filename
WEBAPP_FILENAME='mrl-webapp.war'

# MySQL files location
MYSQL_SCHEMA_FILE='../webapp/src/main/resources/mysql_schema.sql'
MYSQL_CONF_FILE='../webapp/conf/mysql.properties.dist'

# Collection files location
COLLECTION_CONF_FILE='../data-collection/conf/collection.conf.dist'
COLLECTION_CRED_FILE='../data-collection/conf/credentials.file.dist'
COLLECTION_PROJECTS_FILE='../data-collection/conf/ow2_projects.cfg.dist'

# The variable that specify which version need to be deployed.
# Can be either:
# - a tag (e.g. 0.0.7) usually to be deployed in production environment
# - a branch name (e.g. master) usually to be deployed in development environment
REF_TO_DEPLOY=${1:-master}

# Suffix of the GitLab CI job name that built the artifact.
# Job that built a tagged version is named for example jar-data-collection:prod whereas it would be named jar-data-collection:dev if it built a new commit on a branch. See https://gitlab.ow2.org/ow2/mrl/-/blob/master/.gitlab-ci.yml#L53
TARGET_ENV=${2:-prod}

# Check curl requirement
if [ ! "$(command -v curl 2>/dev/null)" ]; then
  echo '!! Warning !! curl tool does not seems to be available !'
fi

# Check that ${WEBAPP_FILENAME} does not already exist in the local folder
[ ! -f ${WEBAPP_FILENAME} ] || { echo "${WEBAPP_FILENAME} already exists in download folder, exiting." ; exit; }
echo 'Downloading' https://${GITLAB_HOST}/api/v4/projects/${PROJECT_ID}/jobs/artifacts/"${REF_TO_DEPLOY}"/raw/webapp/target/${WEBAPP_FILENAME}?job=war-webapp:"${TARGET_ENV}"
curl -sSJLO https://${GITLAB_HOST}/api/v4/projects/${PROJECT_ID}/jobs/artifacts/"${REF_TO_DEPLOY}"/raw/webapp/target/${WEBAPP_FILENAME}?job=war-webapp:"${TARGET_ENV}"
chown -v ${WEBSERVER_USER}:${WEBSERVER_GROUP} ${WEBAPP_FILENAME}
chmod -v 0640 ${WEBAPP_FILENAME}
mkdir -pv ${MRL_WEBAPP_DEPLOYMENT_DIR}
mv -vf ${WEBAPP_FILENAME} ${MRL_WEBAPP_DEPLOYMENT_DIR}

# Download collectdata.sh
curl -sSJLO https://${GITLAB_HOST}/api/v4/projects/${PROJECT_ID}/jobs/artifacts/"${REF_TO_DEPLOY}"/raw/data-collection/bin/collectdata.sh?job=jar-data-collection:"${TARGET_ENV}"

# Download data-collection.jar
curl -sSJLO https://${GITLAB_HOST}/api/v4/projects/${PROJECT_ID}/jobs/artifacts/"${REF_TO_DEPLOY}"/raw/data-collection/target/data-collection.jar?job=jar-data-collection:"${TARGET_ENV}"

# TODO: download lib needed by data-collection.jar

# collectdata.sh
mkdir -pv ${MRL_RUN_DIR}/bin/
mv -vf collectdata.sh ${MRL_RUN_DIR}/bin/
chown -v ${MRL_USER}:${MRL_GROUP} ${MRL_RUN_DIR}/bin/collectdata.sh
chmod -v 0750 ${MRL_RUN_DIR}/bin/collectdata.sh

# data-collection.jar
mv -vf data-collection.jar ${MRL_RUN_DIR}
chown -v ${MRL_USER}:${MRL_GROUP} ${MRL_RUN_DIR}/data-collection.jar
chmod -v 0640 ${MRL_RUN_DIR}/data-collection.jar

# MySQL config
mv -vi ${MYSQL_CONF_FILE} ${MRL_RUN_DIR}/mysql.properties
chown -v ${MRL_USER}:${WEBSERVER_GROUP} ${MRL_RUN_DIR}/mysql.properties
chmod -v 0640 ${MRL_RUN_DIR}/mysql.properties
# Creates OW2_MRL database if not exists, then populates it with table schemas and some data
mysql < ${MYSQL_SCHEMA_FILE} || echo "Importing ${MYSQL_SCHEMA_FILE} failed, please import it !"

# Collection configuration
mkdir -pv ${MRL_RUN_DIR}/conf
mv -vi ${COLLECTION_CONF_FILE} ${MRL_RUN_DIR}/conf/collection.conf
chown -v ${MRL_USER}:${MRL_GROUP} ${MRL_RUN_DIR}/conf/collection.conf
chmod -v 0640 ${MRL_RUN_DIR}/conf/collection.conf
sed -i "/MYSQL_CONFIG_FILENAME=/c\MYSQL_CONFIG_FILENAME=${MRL_RUN_DIR}/mysql.properties" ${MRL_RUN_DIR}/conf/collection.conf

# Collection credentials
mv -vi ${COLLECTION_CRED_FILE} ${MRL_RUN_DIR}/credentials.secret
chown -v ${MRL_USER}:${MRL_GROUP} ${MRL_RUN_DIR}/credentials.secret
chmod -v 0640 ${MRL_RUN_DIR}/credentials.secret

# Collection projects
mv -vi ${COLLECTION_PROJECTS_FILE} ${MRL_RUN_DIR}/conf/ow2_projects.cfg
chown -v ${MRL_USER}:${MRL_GROUP} ${MRL_RUN_DIR}/conf/ow2_projects.cfg
chmod -v 0640 ${MRL_RUN_DIR}/conf/ow2_projects.cfg
sed -i "/include:/c\include: ${MRL_RUN_DIR}/credentials.secret" ${MRL_RUN_DIR}/conf/ow2_projects.cfg
