/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.github;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

import org.ow2.mrl.data.collect.JsonDataHandler;


/**
 * Required data are stats about open bugs
 *  + information about closed bugs(to compute open time, active users...)
 * To retrieve data (eg. on project docdoku-plm), concatenate results from:
 * 1) Recent closed issues (currently limited to 100)
 * curl -X GET -H "Content-Type: application/json"
 *   'https://api.github.com/repos/docdoku/docdoku-plm/issues?state=closed&per_page=100'
 * 2) Recent open issues (currently limited to 100)
 * curl -X GET -H "Content-Type: application/json"
 *   'https://api.github.com/repos/docdoku/docdoku-plm/issues?state=open&per_page=100'
 */
public class GithubHandler extends JsonDataHandler {

	LocalDateTime rDate, cDate, uDate; // Dates of resolution, creation, update
	String lastKey;
	String iid;
	String state;
	int unansweredBugs;
	double timeIntervalCount = 0, updateTimeCount = 0;
	double avgOpenTime = -1D, avgUpdateTime = -1D;
	boolean assignee = false;
	Set<String> assignees = new HashSet<String>();

	public GithubHandler(String project, String jsonPath) {
		super(project, jsonPath);
	}

	/**
	 * Compute average bug open time by iteration.
	 * Average A[n] of values v[1..n] is computed as follows at rank n:
	 * A[n] = 1/n * ( (n-1)*A[n-1] + v[n] )
	 */
	private void computeAverage() {
		if(this.cDate != null) {
			if(this.rDate != null) {
				this.timeIntervalCount ++;
				double interval = Duration.between(cDate, rDate).toDays();
				this.avgOpenTime = ((this.timeIntervalCount-1) * this.avgOpenTime + interval) / this.timeIntervalCount;
			}
			if(this.uDate != null) {
				this.updateTimeCount ++;
				// If updated after being closed, consider close time is the real update time.
				if(this.rDate != null && this.uDate.isAfter(this.rDate))
					this.uDate = this.rDate;
				double interval = Duration.between(cDate, uDate).toMinutes();
				this.avgUpdateTime = ((this.updateTimeCount-1) * this.avgUpdateTime + interval) / this.updateTimeCount;
			}
		}
		this.rDate = this.cDate = this.uDate = null;
	}

	@Override
	public void key(String key) {
	
		/* Example of assignee (or user, each "assignee" is equivalent to a "user") in issue:
		 "assignees":
           [
            {
             "login": "jdoe",
             "id": 868602,
             "node_id": "MDQ6VXNlcjg2ODYwMg==",
             "avatar_url": "",
             "gravatar_id": "",
             "url": "https://api.github.com/users/jdoe",
             [...]
             "type": "User",
             "site_admin": false
            }
           ]
		 */
		if(! this.assignee) this.assignee = "assignees".equals(key) || "user".equals(key);
		else if("creator".equals(key)) this.assignee = false; // Filter out "milestone" objects whose creators are no assignees
		this.lastKey = key;
	}

	@Override
	public void simpleValue(String val) throws Exception {
		if("number".equals(this.lastKey)) {
			this.iid = val.trim();
			this.state = null;
		} else if("closed_at".equals(this.lastKey)) {
			if(! "null".equals(val)) {
				rDate = LocalDateTime.parse(val.substring(0, 19), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
			}
		}
		else if("created_at".equals(this.lastKey)) {
			cDate = LocalDateTime.parse(val.substring(0, 19), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		} else if("updated_at".equals(this.lastKey)) {
			uDate = LocalDateTime.parse(val.substring(0, 19), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		} else if("login".equals(this.lastKey) && this.assignee) {
			this.assignees.add(val.trim());
			this.assignee = false;
		} else if("assignees".equals(this.lastKey)) {
			this.assignee = false; // assignee value is an object, or "null" (if lastKey=assignee, it is null)
		} else if("state".equals(this.lastKey)) {
			this.state = val.trim();
		} else if("comments".equals(this.lastKey) && "open".equals(this.state)) {
			// Unanswered bug = open, no comment
			try {
				if(Integer.parseInt(val.trim()) <= 0) this.unansweredBugs ++;
			} catch(Exception ignore) { }
		} else if("body".equals(this.lastKey)) {
			computeAverage(); // body comes after all dates
		}
	}
	
	/**
	 * Retrieves unanswered bugs count
	 * Unanswered bugs are open bugs with no comment
	 * @return unanswered bugs count
	 */
	public int getUnansweredBugs() {
		return this.unansweredBugs;
	}

	/**
	 * Retrieves average bug open time
	 * Open time is number of days between bug creation and resolution (closed)
	 * @return average bug open time (days)
	 */
	public double getBugOpenTime() {
		return (this.avgOpenTime > 0 ? this.avgOpenTime : -1D);
	}

	/**
	 * Retrieves average bug response time
	 * Response time is number of minutes between bug creation and update
	 * @return average bug response time (minutes)
	 */
	public double getBugResponseTime() {
		return this.avgUpdateTime;
	}

	/**
	 * Retrieves bug tracker active users
	 * Active users are users with bug(s) assigned
	 * @return list of bug tracker active users
	 */
	public Set<String> getBugTrackerActiveUsers() {
		//for(String assignee : assignees) { System.out.println(getProject() + " Github: assignee=[" + assignee + "]"); }
		return this.assignees;
	}

	@Override
	public void endParsing() throws Exception {
		super.endParsing();
		getMetrics().put("ActiveUsers", Double.valueOf(getBugTrackerActiveUsers().size()));
		getMetrics().put("UnansweredBugs", Double.valueOf(getUnansweredBugs()));
		getMetrics().put("Opentime", Double.valueOf(getBugOpenTime()));
		getMetrics().put("AverageResponseTime", Double.valueOf(getBugResponseTime()));
	}

	// Test main program.
	public static void main(String args[]) throws Exception {
		String jsonFile = "/tmp/issues.json";
		if(args.length < 1) {
			System.out.println("Usage: GithubHandler <json-file>");
			System.out.println("Trying " + jsonFile + " as default");
		} else {
			jsonFile = args[0];
		}
		System.out.println("Started: " + new java.util.Date());
	    GithubHandler handler = new GithubHandler("test", jsonFile);
	    handler.run();
		System.out.println("Finished: " + new java.util.Date());
		if(handler.isDone()) {
			System.out.println("Unanswered bugs=" + handler.getUnansweredBugs());
			System.out.println("Average bug open time=" + handler.getBugOpenTime());
			System.out.println("Bug tracker active users=" + handler.getBugTrackerActiveUsers().size());
			System.out.println("Average response time=" + handler.getBugResponseTime());
		}
		else System.out.println("No fixed bug found !!");
	}

}
