/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.sonar;

import org.ow2.mrl.data.collect.JsonDataHandler;

/**
 * Required data are SonarQube metrics
 * To retrieve data, concatenate results from:
 * curl -X GET -H "Content-Type: application/json"
 *   'https://sonarqube.ow2.org/api/measures/component?metricKeys=coverage,blocker_violations,critical_violations,test_success_density,ncloc&componentKey=org.xwiki.platform:xwiki-platform
 */
public class SonarHandler extends JsonDataHandler {

	private String currentMetricName = null;
	String lastKey;

	public SonarHandler(String project, String jsonPath) {
		super(project, jsonPath);
		getMetrics().put("QualitySonarQTestSuccess", -1D);
		getMetrics().put("QualitySonarQTestCoverage", 0D);
		getMetrics().put("BlockerIssue", -1.0);
		getMetrics().put("CriticalIssue", -1.0);
		getMetrics().put("Nloc", 0D);
	}
	
	@Override
	public void endParsing() throws Exception {
		/*
		 * For specific languages (like Perl), Sonar computes no test_success.
		 * When test_success data is missing, we assume 100% test success
		 * (provided that test_coverage > 0: otherwise, no test at all means no success!)
		 */
		if(getMetrics().get("QualitySonarQTestSuccess") < 0) {
			if(getMetrics().get("QualitySonarQTestCoverage") > 0) {
				getMetrics().put("QualitySonarQTestSuccess", 100D);
			} else {
				getMetrics().put("QualitySonarQTestSuccess", 0D);
			}
		}
		super.endParsing();
	}

	@Override
	public void key(String key) {
		this.lastKey = key;
	}

	@Override
	public void simpleValue(String val) throws Exception {
		if("metric".equals(this.lastKey)) {
			this.currentMetricName = val.trim();
		} else if("value".equals(this.lastKey)) {
			if(this.currentMetricName != null) {
				try {
					getMetrics().put(getMetricName(this.currentMetricName), Double.valueOf(val));
				} catch(Exception ignore) { }
				this.currentMetricName = null;
			}
		}
	}
	
	private String getMetricName(String sonarMetric) {
		if("test_success_density".equals(sonarMetric)) {
			return "QualitySonarQTestSuccess";
		} else if("coverage".equals(sonarMetric)) {
			return "QualitySonarQTestCoverage";
		} else if("blocker_violations".equals(sonarMetric)) {
			return "BlockerIssue";
		} else if("critical_violations".equals(sonarMetric)) {
			return "CriticalIssue";
		} else if("ncloc".equals(sonarMetric)) {
			return "Nloc";
		} else {
			return sonarMetric; // Unchanged if unknown
		}
	}
	
	public double getQualitySonarQTestSuccess() {
		return getMetrics().get("QualitySonarQTestSuccess");
	}
	
	public double getQualitySonarQTestCoverage() {
		return getMetrics().get("QualitySonarQTestCoverage");
	}
	
	public double getBlockerIssue() {
		return getMetrics().get("BlockerIssue");
	}
	
	public double getCriticalIssue() {
		return getMetrics().get("CriticalIssue");
	}
	
	public double getNloc() {
		return getMetrics().get("Nloc");
	}

	// Test main program.
	public static void main(String args[]) throws Exception {
		String jsonFile = "/tmp/sonar.json";
		if(args.length < 1) {
			System.out.println("Usage: SonarHandler <json-file>");
			System.out.println("Trying " + jsonFile + " as default");
		} else {
			jsonFile = args[0];
		}
		System.out.println("Started: " + new java.util.Date());
	    SonarHandler handler = new SonarHandler("test", jsonFile);
	    handler.run();
		System.out.println("Finished: " + new java.util.Date());
		if(handler.isDone()) {
			System.out.println("Test coverage=" + handler.getMetrics().get("QualitySonarQTestCoverage"));
			System.out.println("Test success density=" + handler.getMetrics().get("QualitySonarQTestSuccess"));
			System.out.println("Blocker issues=" + handler.getMetrics().get("BlockerIssue"));
			System.out.println("Critical issues=" + handler.getMetrics().get("CriticalIssue"));
			System.out.println("Lines of code=" + handler.getMetrics().get("Nloc"));
		}
		else System.out.println("Could not retrieve Sonar data !!");
	}

}
