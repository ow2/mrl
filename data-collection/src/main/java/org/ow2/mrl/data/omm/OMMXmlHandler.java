/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.omm;

import java.io.FileReader;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import org.ow2.mrl.data.collect.DataHandler;


/**
 * Parse an OMM output file (xml).
 */
public class OMMXmlHandler extends DataHandler {

	public OMMXmlHandler(String project, String xmlPath) {
		super(project, xmlPath);
	}

	@Override
	public void run() throws Exception {
		XMLStreamReader xmlsr = null;
		try {
			XMLInputFactory xmlif = XMLInputFactory.newInstance();
			xmlsr = xmlif.createXMLStreamReader(new FileReader(getDataPath()));
			int eventType, count = 1;
			double total = 0;
			String name = null;
			boolean inValue = false;
			while (xmlsr.hasNext()) {
				eventType = xmlsr.next();
				switch (eventType) {
				case XMLEvent.START_ELEMENT:
					if("property".equalsIgnoreCase(xmlsr.getLocalName())) {
						name = normalizeName(xmlsr.getAttributeValue(0)); // Extract metric name from <property> element, eg. OMM_TST
					} else if("value".equalsIgnoreCase(xmlsr.getLocalName())) {
						inValue = true;
					}
					break;
				case XMLEvent.END_ELEMENT:
					inValue = false;
					break;
				case XMLEvent.CHARACTERS:
					// Retrieve value from <value> element
					if(inValue) {
						double value = 0;
						try {
							value = Double.parseDouble(xmlsr.getText());
						} catch(NumberFormatException e) {
							value = 0;
						}
						total += value;

						// Assume 5 values are available for each OMM variable...
						if(count >= 5) {
							getMetrics().put(name, total); // Sum of all values
							count = 1;
							total = 0;
						} else {
							count ++;
						}
					}
					break;
				default:
					break;
				}
			}
		} finally {
			if (xmlsr != null) try { xmlsr.close(); } catch(Exception ignore) {}
			finalizeData();
		}
	}

	/**
	 * Set missing data to 0
	 * This should be done only when parsing was possible, but no data were found.
	 * (eg. when value element was closed immediately: <value/>, instead of <value>X</value>)
	 */
	private void finalizeData() {
		if(getMetrics().get("OMM_MGT") == null) getMetrics().put("OMM_MGT", 0D);
		if(getMetrics().get("OMM_CTY") == null) getMetrics().put("OMM_CTY", 0D);
		if(getMetrics().get("OMM_DOC") == null) getMetrics().put("OMM_DOC", 0D);
		if(getMetrics().get("OMM_REL") == null) getMetrics().put("OMM_REL", 0D);
		if(getMetrics().get("OMM_COM") == null) getMetrics().put("OMM_COM", 0D);
		if(getMetrics().get("OMM_INF") == null) getMetrics().put("OMM_INF", 0D);
		if(getMetrics().get("OMM_SEC") == null) getMetrics().put("OMM_SEC", 0D);
		if(getMetrics().get("OMM_LIC") == null) getMetrics().put("OMM_LIC", 0D);
		if(getMetrics().get("OMM_DEV") == null) getMetrics().put("OMM_DEV", 0D);
		if(getMetrics().get("OMM_TST") == null) getMetrics().put("OMM_TST", 0D);
	}
	
	/**
	 * Cleanup, and filter old names for backward compatibility (replace with current ones)
	 * @param name The name as found in the data
	 * @return The actual name
	 */
	private String normalizeName(String name) {
		if(name != null && name.trim().length() >= 3) {
			name = name.trim().substring(0,3);
			if("ENV".equals(name)) {
				name = "INF"; // Stands for "infrastructure"
			} else if("OPN".equals(name)) {
				name = "LIC"; // Stands for "licensing"
			}
			return "OMM_" + name;
		}
		else return null;
	}

	// Test main program.
	public static void main(String args[]) throws Exception {
		String xml = "/tmp/OMM.xml";
		if(args.length > 0) xml = args[0];
		System.out.println("Started: " + new java.util.Date());
	    OMMXmlHandler handler = new OMMXmlHandler("test", xml);
	    handler.run();
		System.out.println("Finished: " + new java.util.Date());
		System.out.println(handler);
	}

}
