/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.collect;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

/**
 * Main data handler class for parsing
 */
public abstract class DataHandler {

	private String dataPath;
	private String project;
	private Properties config;
	Map<String, Double> metrics  = new HashMap<String, Double>();

	public DataHandler(String project, String dataPath) {
		this.project = project;
		this.dataPath = dataPath;
	}
	
	public Properties getConfig() {
		return config;
	}

	public void setConfig(Properties config) {
		this.config = config;
	}

	public String getDataPath() {
		return this.dataPath;
	}
	
	public String getProject() {
		return this.project;
	}

	public String getDataDate() {
		File json = new File(dataPath);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.format(new Date(json.lastModified()));
	}
	
	protected Map<String, Double> getMetrics() {
		return this.metrics;
	}

	public abstract void run() throws Exception;
	
	/**
	 * Format and display all metrics available as CSV
	 * project,date_yyyy-MM-dd,metricName,value
	 */
	public String toString() {
		StringBuilder data = new StringBuilder();
		for(Entry<String, Double> entry : this.metrics.entrySet()) {
			Double value = entry.getValue();
			String overwrite = null;
			
			// Metric values can be overwritten by configuration
			// (<projectName>.overwrite.<metricName> property)
			// Example:
			// lemonldap-ng.overwrite.ScancodeRatioNoLic: 10
			if(this.config != null) {
				overwrite = this.config.getProperty(
							getProject() + ".overwrite." + entry.getKey());
				if(overwrite != null) {
					try { // Parse overwritten value
						value = Double.valueOf(overwrite.trim());
					} catch(NumberFormatException e) {
						value = entry.getValue(); // Parse failed, keep raw value
					}
				}
			}
			data.append(getProject() + "," + getDataDate() + "," + entry.getKey() + "," + value + "\n");
		}
		return data.toString();
	}
}
