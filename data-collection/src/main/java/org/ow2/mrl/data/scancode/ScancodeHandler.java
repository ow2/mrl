/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.scancode;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import org.ow2.mrl.data.collect.JsonDataHandler;

/**
 * Parse a scancode output file (json).
 */
public class ScancodeHandler extends JsonDataHandler {

	String excludePrefix, excludeSuffix; // comma-separated list of prefix/suffix to exclude
	String lastKey;
	String path;
	boolean licenseName = false;
	boolean checkFile = false;
	boolean excludeFile = false;
	boolean isFile = true;
	int totalLicensed = 0; // nb of files with license
	int totalFiles = 0; // total nb of files
	Set<String> licenses = new HashSet<String>(); // Set of unique licenses

	public ScancodeHandler(String project, String jsonPath, String excludePrefix, String excludeSuffix) {
		super(project, jsonPath);
		this.excludePrefix = excludePrefix;
		this.excludeSuffix = excludeSuffix;
	}
	
	@Override
	public void key(String key) {
		this.lastKey = key;
		if(key.equals("type")) {
			this.checkFile = ! isExcluded(this.path); // type can be "file" or "directory"
			this.path = null;
		}
		if(this.checkFile && "key".equals(key)) {
			this.licenseName = true;
		}
	}

	@Override
	public void simpleValue(String val) throws Exception {
		if("path".equals(lastKey)) {
			this.path = val;
		}
		if(this.checkFile) {
			if(val.equals("file")) {
				this.isFile = true; // scan license only for files, not dirs
				totalFiles++;
			} else if(this.licenseName && val != null && val.length() > 0 && this.isFile) {
				this.licenseName = false; // count one single license per file !
				this.totalLicensed ++;
				this.licenses.add(val.trim());
				this.checkFile = this.isFile = false;
			}
		}
	}
	
	/**
	 * Check if file is supposed to be excluded, or not.
	 * Exclusion patterns apply to file name only (not path).
	 * @param path Path to check
	 * @return true if excluded, false otherwise
	 */
	private boolean isExcluded(String path) {

		if(path == null || path.length() <= 0) return true; // Exclude empty paths
		
		int pos = path.lastIndexOf(File.separatorChar);
		String toCheck = (pos < 0 ? path : path.substring(pos+1)); // Check only file name

		if(this.excludePrefix != null && this.excludePrefix.length() > 0) {
			StringTokenizer st = new StringTokenizer(this.excludePrefix, ",;");
			while(st.hasMoreTokens()) {
				String pattern = st.nextToken();
				if(toCheck.startsWith(pattern)) return true;
			}
		}
		if(this.excludeSuffix != null && this.excludeSuffix.length() > 0) {
			StringTokenizer st = new StringTokenizer(this.excludeSuffix, ",;");
			while(st.hasMoreTokens()) {
				String pattern = st.nextToken();
				if(toCheck.endsWith(pattern)) return true;
			}
		}

		return false; // No exclusion required
	}
	
	/**
	 * Total number of files analyzed (excluded files do not count)
	 * @return Total files number
	 */
	public double getTotalFiles() {
		return this.totalFiles;
	}
	
	/**
	 * Total number of licensed files (excluded files do not count)
	 * @return Number of licensed files
	 */
	public double getTotalLicensed() {
		return this.totalLicensed;
	}

	/**
	 * Get ratio of files without licenses (total unlicensed / total files)
	 * @return Ratio of unlicensed files
	 */
	public double getNoLicenseRatio() {
		// nb of files without license / total nb of files
		// (totalFiles - totalLicensed)/totalFiles = 1 - totalLicensed/totalFiles
		return 1D - (double)this.totalLicensed / (double)this.totalFiles;
	}

	/**
	 * Get list of license names found in analyzed files
	 * @return Set of unique license names
	 */
	public Set<String> getLicenses() {
		return this.licenses;
	}
	
	@Override
	public void endParsing() throws Exception {
		super.endParsing();
		getMetrics().put("ScancodeTotalFiles", getTotalFiles());
		getMetrics().put("ScancodeTotalLicensed", getTotalLicensed());
		getMetrics().put("ScancodeRatioNoLic", getNoLicenseRatio());
		getMetrics().put("ScancodeUniqueLic", Double.valueOf(getLicenses().size()));
	}

	// Test main program.
	public static void main(String args[]) throws Exception {
		String jsonFile = "/tmp/scancode.json";
		if(args.length < 1) {
			System.out.println("Usage: ScancodeHandler <json-file>");
			System.out.println("Trying " + jsonFile + " as default");
		} else {
			jsonFile = args[0];
		}
		System.out.println("Started: " + new java.util.Date());
	    ScancodeHandler handler = new ScancodeHandler("test", jsonFile, "", null);
		handler.run();
		System.out.println("Finished: " + new java.util.Date());
		if(handler.isDone() && handler.getTotalLicensed() > 0) {
			System.out.println("Total files=" + handler.getTotalFiles());
			System.out.println("Total licensed=" + handler.getTotalLicensed());
			System.out.println("No license ratio=" + handler.getNoLicenseRatio());
			System.out.println("Unique license count=" + handler.getLicenses().size());
			System.out.println("Licenses found:");
			for(String license : handler.getLicenses()) {
				System.out.println("\t" + license);
			}
		}
		else System.out.println("No license found !!");
	}

}
