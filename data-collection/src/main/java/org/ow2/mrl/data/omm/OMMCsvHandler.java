/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.omm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.StringTokenizer;

import org.ow2.mrl.data.collect.DataHandler;


/**
 * Parse an OMM output file (csv).
 */
public class OMMCsvHandler extends DataHandler {

	public OMMCsvHandler(String project, String csvPath) {
		super(project, csvPath);
	}

	@Override
	public void run() throws Exception {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(getDataPath()));
			String line;
			while((line = in.readLine()) != null) {
				String metricName = null;
				double metricValue = 0;
				// Format: omm_section, omm_score, scavaprojectname, ow2_project_name
				// Example: OMM_COM,5,scheduling,proactive
				if(line.startsWith("OMM")) {
					StringTokenizer st = new StringTokenizer(line, ",");
					short tok = 0;
					while(st.hasMoreTokens()) {
						String token = st.nextToken();
						tok++;
						if(tok == 1) metricName = normalizeName(token);
						else if(tok == 2) {
							try {
								metricValue = Double.parseDouble(token);
							} catch(NumberFormatException e) {
								metricValue = 0;
							}
						}
					}
				}
				if(metricName != null) getMetrics().put(metricName, metricValue);
			}
		} finally {
			if (in != null) try { in.close(); } catch(Exception ignore) {}
		}
	}
	
	/**
	 * Cleanup, and filter old names for backward compatibility (replace with current ones)
	 * @param name The name as found in the data
	 * @return The actual name
	 */
	private String normalizeName(String name) {
		if(name != null) {
			name = name.trim();
			if("OMM_ENV".equals(name)) {
				name = "OMM_INF"; // Stands for "infrastructure"
			} else if("OMM_OPN".equals(name)) {
				name = "OMM_LIC"; // Stands for "licensing"
			}
		}
		return name;
	}

	// Test main program.
	public static void main(String args[]) throws Exception {
		String csv = "/tmp/OMM.csv";
		if(args.length > 0) csv = args[0];
		System.out.println("Started: " + new java.util.Date());
	    OMMCsvHandler handler = new OMMCsvHandler("test", csv);
	    handler.run();
		System.out.println("Finished: " + new java.util.Date());
		System.out.println(handler);
	}

}
