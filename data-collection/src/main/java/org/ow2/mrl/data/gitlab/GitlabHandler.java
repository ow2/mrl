/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.gitlab;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

import org.ow2.mrl.data.collect.JsonDataHandler;

/**
 * Required data are stats about open bugs
 *  + information about closed bugs(to compute open time, active users...)
 * To retrieve data (eg. on project ID #125), concatenate results from:
 * 1) Global project issues statistics (counts of all, closed and open issues).
 * curl -X GET -H "Content-Type: application/json"
 *   'https://gitlab.ow2.org/api/v4/projects/125/issues_statistics'
 * 2) Recent closed issues (currently limited to 100)
 * curl -X GET -H "Content-Type: application/json"
 *   'https://gitlab.ow2.org/api/v4/projects/125/issues?state=closed&per_page=100'
 * curl -X GET -H "Content-Type: application/json"
 * 3) Recent open issues (currently limited to 100)
 *   'https://gitlab.ow2.org/api/v4/projects/125/issues?state=openeded&per_page=100'
 * NOTE: Gitlab issues can be gathered at project or group level (eg. /project/125 or /group/109). Both should work.
 */
public class GitlabHandler extends JsonDataHandler {

	LocalDateTime rDate, cDate, uDate;
	String lastKey;
	String iid;
	String state;
	int unansweredBugs;
	double timeIntervalCount = 0, updateTimeCount = 0;
	double avgOpenTime = -1D, avgUpdateTime = -1D;
	boolean assignee = false;
	Set<String> assignees = new HashSet<String>();

	public GitlabHandler(String project, String jsonPath) {
		super(project, jsonPath);
	}

	/**
	 * Compute average bug open time by iteration.
	 * Average A[n] of values v[1..n] is computed as follows at rank n:
	 * A[n] = 1/n * ( (n-1)*A[n-1] + v[n] )
	 */
	private void computeAverage() {
		if(this.cDate != null) {
			if(this.rDate != null) {
				this.timeIntervalCount ++;
				double interval = Duration.between(cDate, rDate).toDays();
				this.avgOpenTime = ((this.timeIntervalCount-1) * this.avgOpenTime + interval) / this.timeIntervalCount;
			}
			if(this.uDate != null) {
				this.updateTimeCount ++;
				// If updated after being closed, consider close time is the real update time.
				if(this.rDate != null && this.uDate.isAfter(this.rDate))
					this.uDate = this.rDate;
				double interval = Duration.between(cDate, uDate).toMinutes();
				this.avgUpdateTime = ((this.updateTimeCount-1) * this.avgUpdateTime + interval) / this.updateTimeCount;
			}
		}
		this.rDate = this.cDate = this.uDate = null;
	}

	@Override
	public void key(String key) {
		//if("opened".contentEquals(key)) unansweredBugs++;
	
		/* Example of assignee (or author or closed_by) or in issue:
		 "assignees":
           [
            {
             "id": "92",
             "name": "John Doe",
             "username": "jdoe",
             "state": "active",
             "avatar_url": "",
             "web_url": "https://gitlab.ow2.org/jdoe"
            }
           ]
		 */
		if(! this.assignee) this.assignee = "author".equals(key) || "assignees".equals(key) || "closed_by".equals(key);
		this.lastKey = key;
	}

	@Override
	public void simpleValue(String val) throws Exception {
		if("iid".equals(this.lastKey)) {
			this.iid = val.trim();
			this.state = null;
		} else if("closed_at".equals(this.lastKey)) {
			if(! "null".equals(val)) {
				rDate = LocalDateTime.parse(val.substring(0, 19), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
			}
			// Note: some old Gitlab issues may be closed with closed_at=null
			// This is probably a Gitlab bug...
			// try workaround if uDate available (and already read)
			else if(uDate != null) rDate = uDate; // This is a hack...
		}
		else if("created_at".equals(this.lastKey)) {
			cDate = LocalDateTime.parse(val.substring(0, 19), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		} else if("updated_at".equals(this.lastKey)) {
			uDate = LocalDateTime.parse(val.substring(0, 19), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		} else if("name".equals(this.lastKey) && this.assignee) {
			this.assignees.add(val.trim());
			this.assignee = false;
		} else if("assignees".equals(this.lastKey)) {
			this.assignee = false; // assignee value is an object, or "null" (if lastKey=assignee, it is null)
		}  else if("state".equals(this.lastKey)) {
			if(this.state == null) this.state = val.trim(); // there are other "state" in embedded objects
		} else if("user_notes_count".equals(this.lastKey)) {
			// Unanswered bug = open, no comment
			if("opened".equals(this.state)) {
				try {
					if(Integer.parseInt(val.trim()) <= 0) this.unansweredBugs ++;
				} catch(Exception ignore) { }
			}

			computeAverage(); // user_notes_count comes after all dates
		}
	}
	
	/**
	 * Retrieves unanswered bugs count
	 * Unanswered bugs are open bugs with no comment
	 * @return unanswered bugs count
	 */
	public int getUnansweredBugs() {
		return this.unansweredBugs;
	}

	/**
	 * Retrieves average bug open time
	 * Open time is number of days between bug creation and resolution (closed)
	 * @return average bug open time (days)
	 */
	public double getBugOpenTime() {
		return (this.avgOpenTime > 0 ? this.avgOpenTime : -1D);
	}

	/**
	 * Retrieves average bug response time
	 * Response time is number of minutes between bug creation and update
	 * @return average bug response time (minutes)
	 */
	public double getBugResponseTime() {
		return this.avgUpdateTime;
	}
	
	/**
	 * Retrieves bug tracker active users
	 * Active users are users with bug(s) assigned
	 * @return list of bug tracker active users
	 */
	public Set<String> getBugTrackerActiveUsers() {
		//for(String assignee : assignees) { System.out.println(assignee); }
		return this.assignees;
	}
	
	@Override
	public void endParsing() throws Exception {
		super.endParsing();
		getMetrics().put("ActiveUsers", Double.valueOf(getBugTrackerActiveUsers().size()));
		getMetrics().put("UnansweredBugs", Double.valueOf(getUnansweredBugs()));
		getMetrics().put("Opentime", Double.valueOf(getBugOpenTime()));
		getMetrics().put("AverageResponseTime", Double.valueOf(getBugResponseTime()));
	}

	// Test main program.
	public static void main(String args[]) throws Exception {
		String jsonFile = "/tmp/issues.json";
		if(args.length < 1) {
			System.out.println("Usage: GitlabHandler <json-file>");
			System.out.println("Trying " + jsonFile + " as default");
		} else {
			jsonFile = args[0];
		}
		System.out.println("Started: " + new java.util.Date());
	    GitlabHandler handler = new GitlabHandler("test", jsonFile);
	    handler.run();
		System.out.println("Finished: " + new java.util.Date());
		if(handler.isDone()) {
			System.out.println("Unanswered bugs=" + handler.getUnansweredBugs());
			System.out.println("Average bug open time=" + handler.getBugOpenTime());
			System.out.println("Bug tracker active users=" + handler.getBugTrackerActiveUsers().size());
			System.out.println("Average response time=" + handler.getBugResponseTime());
		}
		else System.out.println("No fixed bug found !!");
	}

}
