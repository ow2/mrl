/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.collect;

import java.io.BufferedInputStream;
import java.io.FileInputStream;

import com.gibello.icar.json.parser.EventHandler;
import com.gibello.icar.json.parser.JsonParser;

/**
 * JSON data handler for parsing
 */
public class JsonDataHandler extends DataHandler implements EventHandler {

	boolean done = false;

	public JsonDataHandler(String project, String jsonPath) {
		super(project, jsonPath);
	}

	@Override
	public void endParsing() throws Exception {
		this.done = true;
	}
	
	@Override
	public void run() throws Exception {
		this.done = false;
		JsonParser parser = new JsonParser();
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(getDataPath()));
		parser.parse(in, this);
		in.close();
	}

	public boolean isDone() {
		return this.done;
	}

	@Override
	public void endArray() throws Exception {
		// TODO Auto-generated method stub	
	}
	@Override
	public void endObject() throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void key(String arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void simpleValue(String arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void startArray() throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void startObject() throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void startParsing(JsonParser arg0) throws Exception {
		// TODO Auto-generated method stub	
	}
	

}
