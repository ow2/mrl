/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.omm;

import java.io.FileReader;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import org.ow2.mrl.data.collect.DataHandler;


/**
 * Parse an MKT output file (xml).
 */
public class MKTXmlHandler extends DataHandler {

	public MKTXmlHandler(String project, String xmlPath) {
		super(project, xmlPath);
	}

	@Override
	public void run() throws Exception {
		XMLStreamReader xmlsr = null;
		try {
			XMLInputFactory xmlif = XMLInputFactory.newInstance();
			xmlsr = xmlif.createXMLStreamReader(new FileReader(getDataPath()));
			int eventType;
			String name = null;
			boolean inValue = false;
			while (xmlsr.hasNext()) {
				eventType = xmlsr.next();
				switch (eventType) {
				case XMLEvent.START_ELEMENT:
					if("attribute".equalsIgnoreCase(xmlsr.getLocalName())) {
						if("prettyName".equals(xmlsr.getAttributeValue(0)))
							name = normalizeName(xmlsr.getAttributeValue(1)); // Extract metric name from <prettyName> element, eg. MKT_Product
					} else if("value".equalsIgnoreCase(xmlsr.getLocalName())) {
						inValue = true;
					}
					break;
				case XMLEvent.END_ELEMENT:
					inValue = false;
					break;
				case XMLEvent.CHARACTERS:
					// Retrieve value from <value> element
					if(inValue) {
						double value = 0;
						try {
							value = Double.parseDouble(xmlsr.getText());
						} catch(NumberFormatException e) {
							value = 0;
						}
						if(name != null) {
							getMetrics().put(name, value);
							name = null;
						}
					}
					break;
				default:
					break;
				}
			}
		} finally {
			if (xmlsr != null) try { xmlsr.close(); } catch(Exception ignore) {}
			finalizeData();
		}
	}

	/**
	 * Set missing data to 0
	 * This should be done only when parsing was possible, but no data were found.
	 * (eg. when value element was closed immediately: <value/>, instead of <value>X</value>)
	 */
	private void finalizeData() {
		if(getMetrics().get("MKT_Product") == null) getMetrics().put("MKT_Product", 0D);
		if(getMetrics().get("MKT_Community") == null) getMetrics().put("MKT_Community", 0D);
		if(getMetrics().get("MKT_Support") == null) getMetrics().put("MKT_Support", 0D);
		if(getMetrics().get("MKT_Customers") == null) getMetrics().put("MKT_Customers", 0D);
		if(getMetrics().get("MKT_Sales") == null) getMetrics().put("MKT_Sales", 0D);
		if(getMetrics().get("MKT_Finances") == null) getMetrics().put("MKT_Finances", 0D);
		if(getMetrics().get("MKT_Marketing") == null) getMetrics().put("MKT_Marketing", 0D);
	}
	
	/**
	 * Cleanup, and filter old names for backward compatibility (replace with current ones)
	 * @param name The name as found in the data
	 * @return The actual name
	 */
	private String normalizeName(String name) {
		if(name != null) {
			name = name.toLowerCase();
			if(name.contains("product")) {
				return "MKT_Product";
			} else if(name.contains("community")) {
				return "MKT_Community";
			} else if(name.contains("support")) {
				return "MKT_Support";
			} else if(name.contains("customer")) {
				return "MKT_Customers";
			} else if(name.contains("sales")) {
				return "MKT_Sales";
			} else if(name.contains("financial")) {
				return "MKT_Finances";
			} else if(name.contains("market")) {
				return "MKT_Marketing";
			} else return null;
		}
		else return null;
	}

	// Test main program.
	public static void main(String args[]) throws Exception {
		String xml = "/tmp/MKT.xml";
		if(args.length > 0) xml = args[0];
		System.out.println("Started: " + new java.util.Date());
	    MKTXmlHandler handler = new MKTXmlHandler("test", xml);
	    handler.run();
		System.out.println("Finished: " + new java.util.Date());
		System.out.println(handler);
	}

}
