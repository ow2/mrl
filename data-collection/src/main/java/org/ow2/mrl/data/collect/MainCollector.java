/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.collect;

import org.ow2.mrl.data.github.GithubHandler;
import org.ow2.mrl.data.gitlab.GitlabHandler;
import org.ow2.mrl.data.jira.JiraHandler;
import org.ow2.mrl.data.omm.MKTXmlHandler;
import org.ow2.mrl.data.omm.OMMCsvHandler;
import org.ow2.mrl.data.omm.OMMXmlHandler;
import org.ow2.mrl.data.scancode.ScancodeHandler;
import org.ow2.mrl.data.sonar.SonarHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

public class MainCollector {

    /**
     * This tool is design to be execute 3 times, each time running a different phase.
     * Phases are:
     * - Generation of a shell script that will use cURL to get the data from various end point
     * - Execution of the script that will write output result in temporary files
     * - Parsing of the output files and insertion of the data in the database
     */
    private static final int[] phases = new int[3];

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MainCollector.class);

    /**
     * The temporary output dir used to write the generated script and also result of script execution.
     */
    private String outputDir;

    /**
     * Configuration of this tool stored in a configuration file
     */
    Properties config;

    /**
     * Project names retrieve from the configuration file
     */
    Set<String> projects = new HashSet<>();

    /**
     * The data-collection version as set by Maven and stored in the MANIFEST.MF file
     */
    public final static String VERSION =  MainCollector.class.getPackage().getImplementationVersion();

    /**
     * Entry point of the application.
     *
     * @param args program expect path to the configuration file a first argument.
     *             An optional second argument can be provided in order to specify the id (1, 2 or 3) of the phase to execute or a list of id (e.g. 1,3).
     * @throws Exception in case of error.
     */
    public static void main(String[] args) throws Exception {
        if (args.length < 1) throw new Exception("Usage: MainCollector <config-file> [phase-list] or MainCollector --version");

        if ("--version".equals(args[0])) {
            System.out.println("version: " + VERSION);
            System.exit(0);
        }

        // Instantiate the collector with the given configuration file path
        MainCollector collector = new MainCollector(args[0]);

        // Print for debugging information about projects read from the configuration file
        collector.printDebugListOfProjects();

        // Parse the comma-separated phases list (eg. 1,3) as optional parameter
        if (args.length > 1) {
            LOGGER.debug("Phases requested: " + args[1]);
            StringTokenizer st = new StringTokenizer(args[1], ",;");
            int i = 0;
            while (st.hasMoreTokens()) {
                try {
                    if (i < phases.length) phases[i++] = Integer.parseInt(st.nextToken());
                } catch (Exception e) {
                    LOGGER.info("Error while parsing phase argument. Falling back to running default phase 1.", e);
                }
            }
        } else phases[0] = 1;

        LOGGER.debug("Start processing at: {}", new java.util.Date());
        for (int phase : phases) {
            switch (phase) {
                case 1:
                    collector.phase1();
                    break;
                case 2:
                    collector.phase2();
                    break;
                case 3:
                    collector.phase3();
                    break;
                default:
                    break;
            }
        }
        LOGGER.debug("Finish processing at: {}", new java.util.Date());
    }

    private void printDebugListOfProjects() {
        if (LOGGER.isDebugEnabled()) {
            for (String project : projects) {
                LOGGER.debug("Project name: {}:\n\tRepository: {}\n\tIssues API endpoint: {}\n\tIssues API project: {}\n\t" +
                                "Sonarqube Id: {}\n\tScancode data: {}\n\tOMM endpoint or file: {}\n\tMKT endpoint: {}",
                        project, getRepo(project), getIssuesEndpoint(project), getIssuesProject(project),
                        getSonarqubeId(project), getScancodeData(project), getOMMData(project), getMKTEndpoint(project));
            }
        }
    }

    public MainCollector(String configFile) throws IOException {
        LOGGER.debug("Loading configuration from file: {}", configFile);
        this.config = new Properties();
        config.load(new FileInputStream(configFile));
        LOGGER.debug("Configuration loaded from file: {}", configFile);

        // Another properties file can optionally be included
        // Eg. This can be useful for secret data, like credentials
        // (for instance if main file is committed on a Git repo, credentials file
        // can be excluded by a rule in .gitignore).
        String include = config.getProperty("include");
        if (include != null && ! isTemplate(include)) {
            try {
                config.load(new FileInputStream(include));
            } catch (Exception e) { // Ignore with warning
                LOGGER.error("Configuration file: {} specify an additional file to be include: {} but this file can't be loaded", configFile, include, e);
            }
        }

        this.outputDir = config.getProperty("outputDir");
        if (this.outputDir == null || isTemplate(this.outputDir)) {
            this.outputDir = System.getProperty("java.io.tmpdir") + File.separator + "MRL";
            LOGGER.debug("No output directory specified in configuration file, falling back to: {}", outputDir);
        } else {
            LOGGER.debug("Output directory specified in configuration file is: {}", outputDir);
        }

        String projects = this.config.getProperty("projects");
        StringTokenizer st = new StringTokenizer(projects, ",");
        while (st.hasMoreTokens()) {
            this.projects.add(st.nextToken().trim());
        }
    }

    /**
     * Phase 1: generate script to fetch data
     *
     * @throws FileNotFoundException if an error occur while trying to write to the script file.
     */
    public void phase1() throws FileNotFoundException {
        // Create output directory if not already existing
        createOutputDirectory();

        // Path of the generated script file
        File outputFile = new File(this.outputDir + File.separatorChar + "fetch_statistics.sh");
        LOGGER.debug("Generating script to retrieve projects information: {}", outputFile.getAbsolutePath());

        PrintStream out = new PrintStream(new FileOutputStream(outputFile));

        // Write to the file the various instructions needed to get the data
        out.println(generateIssuesScript(true));
        out.println(generateSonarScript(false));
        out.println(generateOMMScript(false));
        out.println(generateScancodeScript(false));
        out.println(generateMKTScript(false));

        // Retrieve MRL thresholds for metrics (CSV format)
        out.println(generateMRLThresholdsScript(false));

        out.close();
        if (!outputFile.setExecutable(true)) {
            LOGGER.error("Failed to make script executable: {}" + outputFile.getAbsolutePath());
        }
    }

    /**
     * Phase 2: run script to fetch data
     *
     * @throws IOException in case execution of the script fail.
     */
    public void phase2() throws IOException {
        //System.out.println("WARNING: Phase 2 not implemented (run "
        //		+ this.outputDir + File.separatorChar + "fetch_statistics.sh manually).");

        Process proc = Runtime.getRuntime()
                .exec(this.outputDir + File.separatorChar + "fetch_statistics.sh");
        BufferedReader read = new BufferedReader(new InputStreamReader(
                proc.getInputStream()));
        try {
            proc.waitFor();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        while (read.ready()) {
            System.out.println(read.readLine());
        }
    }

    /**
     * Phase 3: parse data
     */
    public void phase3() throws FileNotFoundException {
        createOutputDirectory();

        PrintStream out = new PrintStream(new FileOutputStream(this.outputDir + File.separatorChar + "metric_values.csv"));
        DataHandler handler = null;
        for (String project : this.projects) {
            String json;
            // Issues
            String url = getIssuesEndpoint(project);
            if (url.length() > 9) { // At least "http://x.y"
                json = getOutputDir(true) + project + "_issues.json";
                if (url.contains("jira")) {
                    handler = new JiraHandler(project, json);
                } else if (url.contains("github")) {
                    handler = new GithubHandler(project, json);
                }
                if (url.contains("gitlab")) {
                    handler = new GitlabHandler(project, json);
                }

                try {
                    if (handler != null) {
                    	handler.setConfig(this.config);
                        handler.run();
                        out.print(handler.toString());
                    }
                } catch (Exception e) {
                    System.err.println("Missing issues data (" + project + "): " + e.getMessage());
                }
            }

            // Sonar
            json = getOutputDir(true) + project + "_sonar.json";
            handler = new SonarHandler(project, json);
            try {
            	handler.setConfig(this.config);
                handler.run();
                out.print(handler.toString());

            } catch (Exception e) {
                System.err.println("Missing sonar data (" + project + "): " + e.getMessage());
            }

            // Scancode
            json = getOutputDir(true) + project + "_scancode.json";
            String prefixes = trim(this.config.getProperty(project + ".scancode.excludePrefix"));
            String suffixes = trim(this.config.getProperty(project + ".scancode.excludeSuffix"));
            handler = new ScancodeHandler(project, json, prefixes, suffixes);
            try {
            	handler.setConfig(this.config);
                handler.run();
                out.print(handler.toString());
            } catch (Exception e) {
                System.err.println("Missing scancode data (" + project + "): " + e.getMessage());
            }

            // OMM
            String ommPath = getOutputDir(true) + project + "_OMM.xml";
            File ommFile = new File(ommPath);
            if (ommFile.exists()) {
                // Should be XML REST call result...
                handler = new OMMXmlHandler(project, ommPath);
            } else {
                // ... but CSV supported as well
                ommPath = getOutputDir(true) + project + "_OMM.csv";
                handler = new OMMCsvHandler(project, ommPath);
            }

            try {
            	handler.setConfig(this.config);
                handler.run();
                out.print(handler.toString());
            } catch (Exception e) {
                System.err.println("Missing OMM data, either XML or CSV (" + project + "): " + e.getMessage());
            }

            // MKT
            try {
                handler = new MKTXmlHandler(project, getOutputDir(true) + project + "_MKT.xml");
                handler.setConfig(this.config);
                handler.run();
                out.print(handler.toString());
            } catch (Exception e) {
                System.err.println("Missing MKT data (" + project + "): " + e.getMessage());
            }
        }
        out.close();
    }

    private void createOutputDirectory() {
        if ((new File(this.outputDir)).mkdirs()) {
            LOGGER.debug("Creating output directory: {}", outputDir);
        }
    }

    public String getOutputDir(boolean slash) {
        return this.outputDir + (slash ? File.separatorChar : "");
    }

    public Set<String> getProjects() {
        return this.projects;
    }

    public String getRepo(String project) {
        return trim(config.getProperty(project + ".repo"));
    }

    public String getIssuesEndpoint(String project) {
        return trim(config.getProperty(project + ".issues.endpoint"));
    }

    public String getIssuesProject(String project) {
        return trim(config.getProperty(project + ".issues.project"));
    }

    /**
     * Issues credentials, in format "user:password" (like curl "-u" parameter)
     * @param project The project name
     * @return The credentials
     */
    public String getIssuesCredentials(String project) {
        return formatCredentials(config.getProperty(project + ".issues.credentials"));
    }

    /**
     * OMM credentials, in format "user:password" (like curl "-u" parameter)
     * @param project The project name
     * @return The credentials for the project (or global "omm.credentials" if none)
     */
    public String getOMMCredentials(String project) {
        String credentials = null;
        if (project != null) credentials = config.getProperty(project + ".omm.credentials"); // by project
        if (credentials == null) credentials = config.getProperty("omm.credentials"); // global
        return formatCredentials(credentials);
    }

    /**
     * MKT credentials, in format "user:password" (like curl "-u" parameter)
     * @param project The project name
     * @return The credentials for the project (or global "omm.credentials" if none)
     */
    public String getMKTCredentials(String project) {
        String credentials = null;
        if (project != null) credentials = config.getProperty(project + ".mkt.credentials"); // by project
        if (credentials == null) credentials = config.getProperty("mkt.credentials"); // global
        return formatCredentials(credentials);
    }
    
    /**
     * MRL metadata credentials, in format "user:password" (like curl "-u" parameter)
     * @return The credentials for the project (or global "omm.credentials" if none)
     */
    public String getMRLCredentials() {
    	return formatCredentials(config.getProperty("MRL.credentials"));
    }
    
    /**
     * Parse and format credentials (may include special characters)
     * @param credentials The credentials, as read from a configuration file
     * @return Ready-to-use credentials for authentication
     */
    private String formatCredentials(String credentials) {
    	return trim(credentials)
                .replace("`", "\\`").replace("'", "\\'").replace("\"", "\\\"");
    }

    /**
     * Retrieves sonar component
     * @param project The project name
     * @return The sonarqube component (or Id)
     */
    public String getSonarqubeId(String project) {
        return trim(config.getProperty(project + ".sonarqube"));
    }

    /**
     * Retrieves sonar server URL
     * @param project The project name
     * @return The sonarqube server URL (default https://sonarqube.ow2.org)
     */
    public String getSonarqubeServer(String project) {
        String server = trim(config.getProperty(project + ".sonarqube.server"));
        return (server.length() >= 8 ? server : "https://sonarqube.ow2.org"); // 8=length of "http://a"
    }
    
    /**
     * Credentials, in format in format "user:password" (like curl "-u" parameter)
     * @param project The project name
     * @return The credentials
     */
    public String getSonarqubeCredentials(String project) {
    	return trim(config.getProperty(project + ".sonarqube.credentials"))
                .replace("`", "\\`").replace("'", "\\'").replace("\"", "\\\"");
    }

    /**
     * Retrieves Scancode URL or local file path
     * @param project The project name
     * @return Scancode URL or local file path
     */
    public String getScancodeData(String project) {
        return trim(config.getProperty(project + ".scancode"));
    }

    /**
     * Retrieves OMM URL or local file path
     * @param project The project name
     * @return OMM URL or local file path
     */
    public String getOMMData(String project) {
        return trim(config.getProperty(project + ".omm"));
    }

    /**
     * Retrieves OMM URL or local file path
     * @param project The project name
     * @return OMM URL or local file path
     */
    public String getMKTEndpoint(String project) {
        return trim(config.getProperty(project + ".mkt"));
    }
    
    /**
     * Retrieves MRL thresholds endpoint URL
     * @return MRL thresholds endpoint URL
     */
    public String getMRLEndpoint() {
    	String thresholds = config.getProperty("MRL.thresholds");
    	if(thresholds == null || isTemplate(thresholds)) thresholds = config.getProperty("MRL.thresholds.default");
        return trim(thresholds);
    }

    /**
     * Trim a string that can be null
     * @param s String to trim
     * @return Trimmed string, or "" if null
     */
    private String trim(String s) {
        return (s == null ? "" : s.trim());
    }

    /**
     * Complement a URL that may end with "/" with a given path
     * http://url/ + /complement -> http://url/complement
     * http://url + /complement -> http://url/complement
     * http://url + complement -> http://url/complement
     * @param url        Base URL
     * @param complement Path to append
     * @return Complemented URL
     */
    private String complementUrl(String url, String complement) {
        if (complement == null || url == null) return url;
        if (complement.startsWith("/")) complement = complement.substring(1);
        return (url + (url.endsWith("/") ? "" : "/") + complement);
    }

    /**
     * Generate script to gather issues data (all projects)
     * By convention, data files are gathered in configured output dir, and called [project]_issues.json
     * @param header Include shell header in script
     * @return Script to gather issues data (all projects)
     */
    public String generateIssuesScript(boolean header) {
        StringBuilder script = createScriptStringBuilder(header);
        script.append("# Fetch issues data from github/gitlab/jira\n");
        for (String project : this.projects) {
            script.append("echo \"Fetch issues data for ").append(project).append(" into ").append(getOutputDir(true)).append(project).append("_issues.json\"\n");
            script.append(generateIssuesScript(project)).append("\n");
        }
        return script.toString();
    }

    /**
     * Generate script part to gather issues data (single project)
     * @return Script part to gather issues data (single project)
     */
    public String generateIssuesScript(String project) {
        String credentials = getIssuesCredentials(project);
        if (credentials.length() > 0) credentials = " -u " + credentials;
        String url = getIssuesEndpoint(project);
        if (url == null) return null;
        else if (url.contains("jira")) {
            StringBuilder script = new StringBuilder();
            // Retrieve open but not commented issues = substraction of 2 stat queries:
            // JQL for ALL bugs: Type=Bug AND Status=Open AND Project=<projectName>
            // JQL for commented bugs (contain "AM" or "PM"): Type=Bug AND Status=Open AND comment!~'\"M"' AND Project=<projectName>
            script.append("{ curl -X GET").append(credentials).append(" -H \"Content-Type: application/json\" '").append(url).append("?maxResults=0&jql=Type=Bug%20AND%20Status=Open%20AND%20Project=").append(getIssuesProject(project)).append("'; ");
            script.append("curl -X GET").append(credentials).append(" -H \"Content-Type: application/json\" '").append(url).append("?maxResults=0&jql=Type=Bug%20AND%20Status=Open%20AND%20comment!~%27\\\"M\"%27%20AND%20Project=").append(getIssuesProject(project)).append("'; ");
            // Gather latest 100 closed + 100 open bugs max
            script.append("curl -X GET").append(credentials).append(" -H \"Content-Type: application/json\" '").append(url).append("?maxResults=100&jql=Type=Bug%20AND%20Status=Closed%20AND%20Project=").append(getIssuesProject(project)).append("'; ");
            script.append("curl -X GET").append(credentials).append(" -H \"Content-Type: application/json\" '").append(url).append("?maxResults=100&jql=Type=Bug%20AND%20Status=Open%20AND%20Project=").append(getIssuesProject(project)).append("'; } > ").append(getOutputDir(true)).append(project).append("_issues.json");
            return script.toString();
        } else if (url.contains("gitlab")) {
            StringBuilder script = new StringBuilder();
            script.append("{ curl -X GET").append(credentials).append(" -H \"Content-Type: application/json\" '").append(complementUrl(url, getIssuesProject(project))).append("/issues_statistics'; ");
            script.append("curl -X GET").append(credentials).append(" -H \"Content-Type: application/json\" '").append(complementUrl(url, getIssuesProject(project))).append("/issues?state=closed&per_page=100'; ");
            script.append("curl -X GET").append(credentials).append(" -H \"Content-Type: application/json\" '").append(complementUrl(url, getIssuesProject(project))).append("/issues?state=opened&per_page=100'; } > ").append(getOutputDir(true)).append(project).append("_issues.json");
            return script.toString();
        } else if (url.contains("github.com")) {
            StringBuilder script = new StringBuilder();
            script.append("{ curl -X GET").append(credentials).append(" -H \"Content-Type: application/json\" '").append(complementUrl(url, "/issues?state=closed&per_page=100")).append("'; ");
            script.append("curl -X GET").append(credentials).append(" -H \"Content-Type: application/json\" '").append(complementUrl(url, "/issues?state=open&per_page=100")).append("'; } > ").append(getOutputDir(true)).append(project).append("_issues.json");
            return script.toString();
        } else return null;
    }

    /**
     * Generate script to gather SonarQube data (all projects)
     * By convention, data files are gathered in configured output dir, and called [project]_sonar.json
     * @param header Include shell header in script
     * @return Script to gather SonarQube data (all projects)
     */
    public String generateSonarScript(boolean header) {
        StringBuilder script = createScriptStringBuilder(header);

        script.append("# Fetch SonarQube data\n");
        for (String project : this.projects) {
            if (getSonarqubeId(project) != null && getSonarqubeId(project).length() > 0) {
                script.append("echo \"Fetch sonar data for ").append(project).append(" into ").append(getOutputDir(true)).append(project).append("_sonar.json\"\n");
                script.append(generateSonarScript(project)).append("\n");
            }
        }
        return script.toString();
    }

    /**
     * Generate script part to gather SonarQube data (single project
     * @return Script part to gather SonarQube data (single project)
     */
    public String generateSonarScript(String project) {
        String component = getSonarqubeId(project);
        String server = getSonarqubeServer(project);
        String credentials = getSonarqubeCredentials(project);
        if (credentials.length() > 0) credentials = " -u " + credentials;
        if (component == null) return null;
        else {
            return "curl -X GET" + credentials + " -H \"Content-Type: application/json\" '"
                    + complementUrl(server, "/api/measures/component?metricKeys=coverage,blocker_violations,critical_violations,test_success_density,ncloc&component=")
                    + component + "' > " + getOutputDir(true) + project + "_sonar.json";
        }
    }

    /**
     * Generate script to gather OMM data (all projects)
     * By convention, data files are gathered in configured output dir, and called [project]_OMM.xml (or .csv)
     * @param header Include shell header and creation of output directory in script
     * @return Script to gather OMM data (all projects)
     */
    public String generateOMMScript(boolean header) {
        LOGGER.debug("Starting writing instruction in the script to get OMM data");

        StringBuilder script = createScriptStringBuilder(header);

        script.append("# Fetch OMM data\n");
        for (String project : this.projects) {
            String pathOrUrl = getOMMData(project);
            if (pathOrUrl != null && pathOrUrl.trim().length() > 0) {
                script.append("echo \"Fetch OMM data for ").append(project).append(" into ").append(getOutputDir(true)).append(project).append("_OMM.xml (or csv)\"\n");
                script.append(generateOMMScript(project)).append("\n");
            }
        }

        LOGGER.debug("Finishing writing instruction in the script to get OMM data");
        return script.toString();
    }

    private StringBuilder createScriptStringBuilder(boolean header) {
        StringBuilder script = new StringBuilder(header ? "#!/bin/bash\n" : "");
        if (header) {
            script.append("mkdir -p ").append(this.outputDir).append("\n");
        }
        return script;
    }

    /**
     * Generate script part to gather OMM data (single project)
     * @return Script part to gather OMM data (single project)
     */
    public String generateOMMScript(String project) {
        LOGGER.debug("Generating script instruction to get OMM data for: {} project. Output folder is: {}", project, getOutputDir(true));
        String credentials = getOMMCredentials(project);
        if (credentials.length() > 0) credentials = " -u " + credentials;
        String pathOrUrl = getOMMData(project).trim();
        String mtype = (pathOrUrl.toLowerCase().endsWith(".csv") ? "csv" : "xml");
        if (pathOrUrl.startsWith("http://") || pathOrUrl.startsWith("https://")) {
            return "curl -L -X GET" + credentials + " -H \"Content-Type: text/" + mtype + "\" '"
                    + pathOrUrl + "' > " + getOutputDir(true) + project + "_OMM." + mtype;
        } else {
            return "cp " + pathOrUrl + " " + getOutputDir(true) + project + "_OMM." + mtype;
        }
    }

    /**
     * Generate script to gather MKT data (all projects)
     * By convention, data files are gathered in configured output dir, and called [project]_MKT.xml
     * @param header Include shell header in script
     * @return Script to gather MKT data (all projects)
     */
    public String generateMKTScript(boolean header) {
        StringBuilder script = createScriptStringBuilder(header);
        script.append("# Fetch MKT data\n");
        for (String project : this.projects) {
            String endpoint = getMKTEndpoint(project);
            if (endpoint != null && endpoint.trim().length() > 0) {
                script.append("echo \"Fetch MKT data for ").append(project).append(" into ").append(getOutputDir(true)).append(project).append("_MKT.xml\"\n");
                script.append(generateMKTScript(project)).append("\n");
            }
        }
        return script.toString();
    }

    /**
     * Generate script part to gather MKT data (single project)
     * @return Script part to gather MKT data (single project)
     */
    public String generateMKTScript(String project) {
        String credentials = getOMMCredentials(project);
        if (credentials.length() > 0) credentials = " -u " + credentials;
        return "curl -L -X GET" + credentials + " -H \"Content-Type: text/xml\" '"
                + getMKTEndpoint(project) + "' > " + getOutputDir(true) + project + "_MKT.xml";
    }

    /**
     * Generate script to gather Scancode data (all projects)
     * By convention, data files are gathered in configured output dir, and called [project]_scancode.json
     * @param header Include shell header in script
     * @return Script to gather Scancode data (all projects)
     */
    public String generateScancodeScript(boolean header) {
        StringBuilder script = createScriptStringBuilder(header);
        script.append("# Fetch Scancode data\n");
        for (String project : this.projects) {
            String pathOrUrl = getScancodeData(project);
            if (pathOrUrl != null && pathOrUrl.trim().length() > 0) {
                script.append("echo \"Fetch Scancode data for ").append(project).append(" into ").append(getOutputDir(true)).append(project).append("_scancode.json\"\n");
                script.append(generateScancodeScript(project)).append("\n");
            }
        }
        return script.toString();
    }

    /**
     * Generate script part to gather Scancode data (single project)
     * @return Script part to gather Scancode data (single project)
     */
    public String generateScancodeScript(String project) {
        String pathOrUrl = getScancodeData(project).trim();
        if (pathOrUrl.startsWith("http://") || pathOrUrl.startsWith("https://")) {
            return "curl -L -X GET -H \"Content-Type: application/json\" '"
                    + pathOrUrl + "' > " + getOutputDir(true) + project + "_scancode.json";
        } else {
            return "cp " + pathOrUrl + " " + getOutputDir(true) + project + "_scancode.json";
        }
    }
    
    /**
     * Generate script to gather MRL thresholds (CSV format).
     * Errors are ignored.
     * @param header Include shell header in script
     * @return Script to gather MRL thresholds
     */
    public String generateMRLThresholdsScript(boolean header) {
    	StringBuilder script = createScriptStringBuilder(header);
    	String endpoint = getMRLEndpoint();
    	if(endpoint.length() > 0) { // No empty string
    		String credentials = getMRLCredentials();
    		if (credentials.length() > 0) credentials = " -u " + credentials;
    		script.append("# Fetch MRL thresholds metadata\n");
    		// Ignore errors: see "||true" at the end
    		script.append("curl -L -X GET" + credentials + " -H \"Content-Type: text/xml\" '"
        		+ getMRLEndpoint() + "' > " + getOutputDir(true) + "MRL_thresholds.csv ||true\n");
    	} else {
    		script.append("# Warning: no MRL thresholds retrieved (check MRL.thresholds property)\n");
    	}
    	return script.toString();
    }
    
    /**
     * Check for property values being templates.
     * Templates are values like "<path to directory>", to be substituted.
     * @param propValue The value to check
     * @return true if the value is a template, false otherwise
     */
    private boolean isTemplate(String propValue) {
    	return (propValue != null && propValue.trim().startsWith("<"));
    }
}
