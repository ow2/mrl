/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.jira;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

import org.ow2.mrl.data.collect.JsonDataHandler;

/**
 * Required data are stats about open bugs
 *  + information about closed bugs(to compute open time, active users...)
 * To retrieve data, concatenate results from:
 * 1) Unanswered bugs (open + not commented bugs)
 * curl -X GET -H "Content-Type: application/json"
 *   'http://jira.xwiki.org/rest/api/2/search?maxResults=0&jql=Type=Bug%20AND%20Status=Open%20AND%20issueFunction%20not%20in%20commented("")%20AND%20Project=XWIKI'
 * 2) Recent closed bugs (currently limited to 100)
 * curl -X GET -H "Content-Type: application/json"
 *   'http://jira.xwiki.org/rest/api/2/search?maxResults=100&jql=Type=Bug%20AND%20Status=Closed%20AND%20Project=XWIKI'
 * 3) Recent open bugs (currently limited to 100)
 * curl -X GET -H "Content-Type: application/json"
 *   'http://jira.xwiki.org/rest/api/2/search?maxResults=100&jql=Type=Bug%20AND%20Status=Open%20AND%20Project=XWIKI'
 * WARNING: Project must be embedded in JQL query.
 *   Adding project (as below) as a specific parameter instead of part of JQL gives irrelevant results (?):
 *   (wrong request) https://jira.xwiki.org/rest/api/2/search?project=XWIKI&maxResults=0&jql=Type=Bug%20AND%20Status=Open
 */
public class JiraHandler extends JsonDataHandler {

	int level = 0, issueLevel = 0;
	int unansweredBugs = -1;
	boolean inStatQueries = true;
	LocalDateTime rDate, cDate, uDate;
	String lastKey;
	String iid;
	double openTimeCount = 0, updateTimeCount = 0;
	double avgOpenTime = -1D, avgUpdateTime = -1D;
	boolean assignee = false;
	Set<String> assignees = new HashSet<String>();

	public JiraHandler(String project, String jsonPath) {
		super(project, jsonPath);
	}

	/**
	 * Compute average bug open/update time by iteration.
	 * Average A[n] of values v[1..n] is computed as follows at rank n:
	 * A[n] = 1/n * ( (n-1)*A[n-1] + v[n] )
	 */
	private void computeAverage() {
		if(this.cDate != null) {
			if(this.rDate != null) {
				this.openTimeCount ++;
				double interval = Duration.between(cDate, rDate).toDays();
				this.avgOpenTime = ((this.openTimeCount-1) * this.avgOpenTime + interval) / this.openTimeCount;
			}
			if(this.uDate != null) {
				this.updateTimeCount ++;
				// If updated after being closed, consider close time is the real update time.
				if(this.rDate != null && this.uDate.isAfter(this.rDate))
					this.uDate = this.rDate;
				double interval = Duration.between(cDate, uDate).toMinutes();
				this.avgUpdateTime = ((this.updateTimeCount-1) * this.avgUpdateTime + interval) / this.updateTimeCount;
			}
		}
		this.rDate = this.cDate = this.uDate = null;
	}

	@Override
	public void startObject() throws Exception {
		this.level++;
	}
	@Override
	public void endObject() throws Exception {
		if(this.level == this.issueLevel) computeAverage();
		this.level--;
	}
	
	@Override
	public void key(String key) {
		if(! this.assignee) this.assignee = "assignee".equals(key);
		if("expand".equals(key)) inStatQueries = false;
		this.lastKey = key;
	}

	@Override
	public void simpleValue(String val) throws Exception {
		if("id".equals(this.lastKey) && this.level <= 2) {
			this.issueLevel = this.level; // We are in an issue
			this.iid = val.trim();
			//System.out.println("issue #" + this.iid);
		} if("resolutiondate".equals(this.lastKey)) {
			if(! "null".equals(val)) {
				rDate = LocalDateTime.parse(val.substring(0, 19), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
			}
		}
		else if("created".equals(this.lastKey)) {
			cDate = LocalDateTime.parse(val.substring(0, 19), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		} else if("updated".equals(this.lastKey) && ! "null".equals(val)) {
			uDate = LocalDateTime.parse(val.substring(0, 19), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		} else if("name".equals(this.lastKey) && this.assignee) {
			this.assignees.add(val.trim());
			this.assignee = false;
		} else if("assignee".equals(this.lastKey)) {
			this.assignee = false; // assignee value is an object, or "null" (if lastKey=assignee, it is null)
		} else if(inStatQueries && "total".equals(this.lastKey)) {
			// 2 JQL queries returning stats (maxResults=0):
			// 1st one to get all bugs: Type=Bug AND Status=Open
			// 2nd one to get commented bugs (contain "AM" or "PM"): Type=Bug AND Status=Open AND comment!~'\"M"'
			// Unanswered bugs = all - commented
			// Sample HTTP GET args:
			// maxResults=0&jql=Type=Bug%20AND%20Status=Open%20AND%20comment!~%27\"M"%27%20AND%20Project=XWIKI
			int stat;
			try {
				stat = Integer.parseInt(val);
			} catch(Exception e) {
				stat = 0;
			}
			if(this.unansweredBugs < 0) {
				this.unansweredBugs = stat; // Should be all bugs (1st query)
			} else {
				this.unansweredBugs -= stat; // Substract commented bugs (2nd query)
			}
		}
	}
	
	/**
	 * Retrieves unanswered bugs count
	 * Unanswered bugs are open bugs with no comment
	 * @return unanswered bugs count
	 */
	public int getUnansweredBugs() {
		return this.unansweredBugs;
	}

	/**
	 * Retrieves average bug open time
	 * Open time is number of days between bug creation and resolution
	 * @return average bug open time (days)
	 */
	public double getBugOpenTime() {
		return this.avgOpenTime;
	}
	
	/**
	 * Retrieves average bug response time
	 * Response time is number of minutes between bug creation and update
	 * @return average bug response time (minutes)
	 */
	public double getBugResponseTime() {
		return this.avgUpdateTime;
	}
	
	/**
	 * Retrieves bug tracker active users
	 * Active users are users with bug(s) assigned
	 * @return list of bug tracker active users
	 */
	public Set<String> getBugTrackerActiveUsers() {
		//for(String assignee : assignees) { System.out.println(assignee); }
		return this.assignees;
	}

	@Override
	public void endParsing() throws Exception {
		super.endParsing();
		getMetrics().put("ActiveUsers", Double.valueOf(getBugTrackerActiveUsers().size()));
		getMetrics().put("UnansweredBugs", Double.valueOf(getUnansweredBugs()));
		getMetrics().put("Opentime", Double.valueOf(getBugOpenTime()));
		getMetrics().put("AverageResponseTime", Double.valueOf(getBugResponseTime()));
	}

	// Test main program.
	public static void main(String args[]) throws Exception {
		String jsonFile = "/tmp/issues.json";
		if(args.length < 1) {
			System.out.println("Usage: JiraHandler <json-file>");
			System.out.println("Trying " + jsonFile + " as default");
		} else {
			jsonFile = args[0];
		}
		System.out.println("Started: " + new java.util.Date());
	    JiraHandler handler = new JiraHandler("test", jsonFile);
	    handler.run();
		System.out.println("Finished: " + new java.util.Date());
		if(handler.isDone()) {
			System.out.println("Unanswered bugs=" + handler.getUnansweredBugs());
			System.out.println("Average bug open time=" + handler.getBugOpenTime());
			System.out.println("Average bug response time=" + handler.getBugResponseTime());
			System.out.println("Bug tracker active users=" + handler.getBugTrackerActiveUsers().size());
		}
		else System.out.println("No fixed bug found !!");
	}

}
