/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.github;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class GithubHandlerTest {

	@Test
	void testRun() {
		GithubHandler handler = new GithubHandler("test",
				"src/test/resources/issues/github_issues.json");
		try {
			handler.run();
			assertEquals(16.4, handler.getBugOpenTime());
			assertEquals(37956, handler.getBugResponseTime());
			assertEquals(8, handler.getBugTrackerActiveUsers().size());
			assertEquals(3, handler.getUnansweredBugs());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
