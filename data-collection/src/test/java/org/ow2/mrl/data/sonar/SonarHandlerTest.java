/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.sonar;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SonarHandlerTest {

	@Test
	void testRun() {
		SonarHandler handler = new SonarHandler("test",
			"src/test/resources/sonar/sonar1.json");
		try {
			handler.run();
			assertEquals(100, handler.getQualitySonarQTestSuccess());
			assertEquals(96.6, handler.getQualitySonarQTestCoverage());
			assertEquals(0, handler.getBlockerIssue());
			assertEquals(32, handler.getCriticalIssue());
			assertEquals(10276, handler.getNloc());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

}
