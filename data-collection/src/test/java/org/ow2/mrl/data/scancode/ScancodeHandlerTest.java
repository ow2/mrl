/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.scancode;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

class ScancodeHandlerTest {

	@Test
	void testRun() {
		ScancodeHandler handler = new ScancodeHandler("test",
			"src/test/resources/scancode/scancode1.json", null, null);
		try {
			handler.run();
			assertEquals(12, handler.getTotalFiles());
			assertEquals(3, handler.getTotalLicensed());
			assertEquals(2, handler.getLicenses().size());
			assertEquals(0.75, handler.getNoLicenseRatio());
			Set<String> licenses = new HashSet<String>();
			licenses.add("agpl-3.0");
			licenses.add("lgpl-3.0");
			assertIterableEquals(licenses, handler.getLicenses());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void testRunExclude() {
		// Exclude file(s) starting with ("2016" or "2017")
		// or ending with (".old" or "330.txt")
		ScancodeHandler handler = new ScancodeHandler("test",
			"src/test/resources/scancode/scancode1.json",
			"2016,2017", ".old,330.txt");
		try {
			handler.run();
			assertEquals(10, handler.getTotalFiles()); // 2 files excluded
			assertEquals(3, handler.getTotalLicensed());
			assertEquals(2, handler.getLicenses().size());
			assertEquals(0.7, handler.getNoLicenseRatio());
			Set<String> licenses = new HashSet<String>();
			licenses.add("agpl-3.0");
			licenses.add("lgpl-3.0");
			assertIterableEquals(licenses, handler.getLicenses());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
