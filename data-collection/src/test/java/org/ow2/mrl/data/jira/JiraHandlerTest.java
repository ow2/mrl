/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.jira;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class JiraHandlerTest {

	@Test
	void testRun() {
		JiraHandler handler = new JiraHandler("test",
				"src/test/resources/issues/jira_issues.json");
		try {
			handler.run();
			assertEquals(24.29, round2(handler.getBugOpenTime()));
			assertEquals(256219.77, round2(handler.getBugResponseTime()));
			assertEquals(8, handler.getBugTrackerActiveUsers().size());
			assertEquals(33, handler.getUnansweredBugs());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Round double to 2 decimals
	 * @param d double to round
	 * @return rounded value (2 decimals)
	 */
	private double round2(double d) {
		return Math.round(d * 100) / 100D;
	}
}
