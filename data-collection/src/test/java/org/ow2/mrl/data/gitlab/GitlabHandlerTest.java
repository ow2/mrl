/*
 * SPDX-FileCopyrightText: 2021 OW2
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package org.ow2.mrl.data.gitlab;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class GitlabHandlerTest {

	@Test
	void testRun() {
		GitlabHandler handler = new GitlabHandler("test",
				"src/test/resources/issues/gitlab_issues.json");
		try {
			handler.run();
			assertEquals(2171, handler.getBugOpenTime());
			assertEquals(5128339, handler.getBugResponseTime());
			assertEquals(5, handler.getBugTrackerActiveUsers().size());
			assertEquals(2, handler.getUnansweredBugs());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

}
