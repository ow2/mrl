#!/bin/bash

# SPDX-FileCopyrightText: 2021 OW2
#
# SPDX-License-Identifier: Apache-2.0

# infer BASE_DIR from the current script's director
BIN_DIR=$(dirname $0)
BASE_DIR=${BIN_DIR%%/bin}
CONF_DIR=${BASE_DIR}/conf

JAR_FILENAME='data-collection.jar'
CONFIG_FILENAME='collection.conf'
CONFIG_FULLPATH="${CONF_DIR}/${CONFIG_FILENAME}"

if [ -e "${CONFIG_FULLPATH}" ]; then
  source ${CONFIG_FULLPATH}
else
  echo "Config file not found (${CONFIG_FULLPATH}). Exiting."
  exit 1
fi

# This function would trim any whitespace from provided argument
# and return the trimmed string
# https://stackoverflow.com/a/3352015/1876017
trim() {
    local var="$*"
    # remove leading whitespace characters
    var="${var#"${var%%[![:space:]]*}"}"
    # remove trailing whitespace characters
    var="${var%"${var##*[![:space:]]}"}"
    printf '%s' "$var"
}

# check that all required files are available (bin and conf)
for _FILE in ${CONF_DIR}/ow2_projects.cfg ${MYSQL_CONFIG_FILENAME} ${BASE_DIR}/${JAR_FILENAME}; do
  [ -e ${_FILE} ] && continue || { echo "${_FILE} is missing, exit." ; exit; }
done

# extract MySQL credentials from webapp config

_PROPERTY=$(grep ^user: ${MYSQL_CONFIG_FILENAME})
MYSQL_USER=$(trim ${_PROPERTY##*:})
_PROPERTY=$(grep ^database: ${MYSQL_CONFIG_FILENAME})
MYSQL_DB=$(trim ${_PROPERTY##*:})
_PROPERTY=$(grep ^password: ${MYSQL_CONFIG_FILENAME})
MYSQL_PASSWORD=$(trim ${_PROPERTY##*:})
unset _PROPERTY

# check if all MYSQL variables contains a value, otherwise set the value to 'notset'
for PROPERTY in ${MYSQL_USER:-notset} ${MYSQL_DB:-notset} ${MYSQL_PASSWORD:-notset}; do
  [ "${PROPERTY}" == 'notset' ] && { echo "one of the MYSQL_ property cannot be set, exit." ; exit; } || continue
done

# Extract collector's output folder from its configuration file (.cfg)
_OUTPUT=$(grep ^outputDir ${CONF_DIR}/ow2_projects.cfg)
# extract the value
COLLECTOR_OUTPUT_DIR=$(trim ${_OUTPUT##*:})

JAR_FULL_PATH=${BASE_DIR}/${JAR_FILENAME}

# Generate script to fetch data
# (according to data sources configuration file)
java -jar ${JAR_FULL_PATH} ${CONF_DIR%%/}/ow2_projects.cfg 1

# Fetch data (call fetch_statistics.sh from path specified by configuration file)
${COLLECTOR_OUTPUT_DIR}/fetch_statistics.sh

# Parse data and generate CSV with metrics
# (metric_values.csv in path specified by configuration file)
java -jar ${JAR_FULL_PATH} ${CONF_DIR%%/}/ow2_projects.cfg 3

# Load data in MySQL database
mysql -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DB -Bse "LOAD DATA LOCAL INFILE '${COLLECTOR_OUTPUT_DIR}/metric_values.csv' REPLACE INTO TABLE RawData FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n';"

# Load MRL metadata if present
if [ -e ${COLLECTOR_OUTPUT_DIR}/MRL_thresholds.csv ]
then
  mysql -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DB -Bse "DROP TABLE IF EXISTS SpareMetric;"
  mysql -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DB -Bse "CREATE TABLE SpareMetric as select * from Metric;"
  mysql -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DB -Bse "LOAD DATA LOCAL INFILE '${COLLECTOR_OUTPUT_DIR}/MRL_thresholds.csv' REPLACE INTO TABLE Metric FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' (MetricName,Threshold1,Threshold2,Threshold3,Threshold4,Threshold5,Reverse);"
fi
